const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	auth_login,
} = require('../controllers/auth')

// create application/json parser
//var jsonParser = bodyParser.json()

router.post("/auth/auth_login", auth_login);

module.exports = router;