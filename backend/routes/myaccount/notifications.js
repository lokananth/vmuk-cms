const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 

    web_update_personal_details_myaccount,


} = require('../../controllers/myaccount/notifications')

const {
	requireSignIn
} = require('../../controllers/auth');


router.post("/myaccount/notifications/web_update_personal_details_myaccount",requireSignIn,web_update_personal_details_myaccount);



module.exports = router;