const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    TopupProcessByCredidCard,
    SendOnlySMS,
    SubscribeDmukBundle,
    GetDMUKBundleinfo,
    GetBundleInfoDetails,
    GetmyaccountCheckBundleEligibility,
    Web_TopUpAmount_SP_myaccount,
    DeleteCCUpdates,
    TopupExisting,
    GetFreeSimOrderProcessStep2,
    AddNewOrder,
    UpdateAutoTopupStatus,
    webMyaccountEnableAutoTopup,
    Websitemyaccountcancelautorenew,
} = require('../../controllers/myaccount/topupBundles')

const {
	requireSignIn
} = require('../../controllers/auth');

router.post("/myaccount/topupBundles/TopupProcessByCredidCard",requireSignIn,TopupProcessByCredidCard);
router.post("/myaccount/topupBundles/SendOnlySMS",requireSignIn,SendOnlySMS);
router.post("/myaccount/topupBundles/SubscribeDmukBundle",requireSignIn,SubscribeDmukBundle);
router.post("/myaccount/topupBundles/GetDMUKBundleinfo",requireSignIn,GetDMUKBundleinfo);
router.post("/myaccount/topupBundles/GetBundleInfoDetails",requireSignIn,GetBundleInfoDetails);
router.post("/myaccount/topupBundles/GetmyaccountCheckBundleEligibility",requireSignIn,GetmyaccountCheckBundleEligibility);
router.post("/myaccount/topupBundles/Web_TopUpAmount_SP_myaccount",requireSignIn,Web_TopUpAmount_SP_myaccount);
router.post("/myaccount/topupBundles/DeleteCCUpdates",requireSignIn,DeleteCCUpdates);
router.post("/myaccount/topupBundles/TopupExisting",requireSignIn,TopupExisting);
router.post("/myaccount/topupBundles/GetFreeSimOrderProcessStep2",requireSignIn,GetFreeSimOrderProcessStep2);
router.post("/myaccount/topupBundles/AddNewOrder",requireSignIn,AddNewOrder);
router.post("/myaccount/topupBundles/UpdateAutoTopupStatus",requireSignIn,UpdateAutoTopupStatus);
router.post("/myaccount/topupBundles/webMyaccountEnableAutoTopup",requireSignIn,webMyaccountEnableAutoTopup);
router.post("/myaccount/topupBundles/Websitemyaccountcancelautorenew",requireSignIn,Websitemyaccountcancelautorenew);

module.exports = router;