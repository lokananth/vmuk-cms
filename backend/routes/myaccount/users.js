const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 

    get_brand_type_by_mobileno,
    personal_login_myaccount,

} = require('../../controllers/myaccount/users')

const {
	requireSignIn
} = require('../../controllers/auth');


router.post("/myaccount/users/get_brand_type_by_mobileno",requireSignIn,get_brand_type_by_mobileno);
router.post("/myaccount/users/personal_login_myaccount",requireSignIn,personal_login_myaccount);


module.exports = router;