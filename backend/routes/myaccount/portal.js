const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 

    webmyaccountcheckcardno,
    GetCCUpdates,
    web_get_personal_details_myaccount,
    GetCcardStatus,
    InsertCCUpdates,
    GetRegisterNumberList,
    GetOutstandingAmount,
    GetRenewalInfo,
    GetBundlePackBalance,
    Websitemyaccountinsertemaillog,

} = require('../../controllers/myaccount/portal')

const {
	requireSignIn
} = require('../../controllers/auth');


router.post("/myaccount/portal/webmyaccountcheckcardno",requireSignIn,webmyaccountcheckcardno);
router.post("/myaccount/portal/GetCCUpdates",requireSignIn,GetCCUpdates);
router.post("/myaccount/portal/web_get_personal_details_myaccount",requireSignIn,web_get_personal_details_myaccount);
router.post("/myaccount/portal/GetCcardStatus",requireSignIn,GetCcardStatus);
router.post("/myaccount/portal/InsertCCUpdates",requireSignIn,InsertCCUpdates);
router.post("/myaccount/portal/GetRegisterNumberList",requireSignIn,GetRegisterNumberList);
router.post("/myaccount/portal/GetOutstandingAmount",requireSignIn,GetOutstandingAmount);
router.post("/myaccount/portal/GetRenewalInfo",requireSignIn,GetRenewalInfo);
router.post("/myaccount/portal/GetBundlePackBalance",requireSignIn,GetBundlePackBalance);
router.post("/myaccount/portal/Websitemyaccountinsertemaillog",requireSignIn,Websitemyaccountinsertemaillog);


module.exports = router;