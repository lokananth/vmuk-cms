const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    web_rfr_transaction_list_myaccount,
    web_rfr_avail_credit_info_myaccount,
    web_get_list_subscribed_bundle_myaccount
} = require('../../controllers/myaccount/referafriend')

const {
	requireSignIn
} = require('../../controllers/auth');

router.post("/myaccount/referafriend/web_rfr_transaction_list_myaccount",
			requireSignIn, 
			web_rfr_transaction_list_myaccount);
router.post("/myaccount/referafriend/web_rfr_avail_credit_info_myaccount",
			requireSignIn, 
			web_rfr_avail_credit_info_myaccount);
router.post("/myaccount/referafriend/web_get_list_subscribed_bundle_myaccount",
			requireSignIn, 
			web_get_list_subscribed_bundle_myaccount);

module.exports = router;