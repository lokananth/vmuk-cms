const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 

    get_payment_history,
    get_brand_type_by_mobileno,
    get_subscribed_info,
    get_paym_plan_id,
    get_myaccount_subsrcibed_bundle,
    personal_login_myaccount,

} = require('../../controllers/myaccount/account')

const {
	requireSignIn
} = require('../../controllers/auth');


router.post("/myaccount/account/get_payment_history",requireSignIn,get_payment_history);
router.post("/myaccount/account/get_brand_type_by_mobileno",requireSignIn,get_brand_type_by_mobileno);
router.post("/myaccount/account/get_subscribed_info",requireSignIn,get_subscribed_info);
router.post("/myaccount/account/get_paym_plan_id",requireSignIn,get_paym_plan_id);
router.post("/myaccount/account/get_myaccount_subsrcibed_bundle",requireSignIn,get_myaccount_subsrcibed_bundle);
router.post("/myaccount/account/personal_login_myaccount",requireSignIn,personal_login_myaccount);


module.exports = router;