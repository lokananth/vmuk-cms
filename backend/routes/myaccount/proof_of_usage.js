const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    get_call_history,
    Webgetmaimeidtl,


} = require('../../controllers/myaccount/proof_of_usage')

const {
	requireSignIn
} = require('../../controllers/auth');

router.post("/myaccount/proof_of_usage/get_call_history",requireSignIn,get_call_history);
router.post("/myaccount/proof_of_usage/Webgetmaimeidtl",requireSignIn,Webgetmaimeidtl);


module.exports = router;