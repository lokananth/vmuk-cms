const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    webmyaccount_validate_iccid_info,
    website_myaccount_sim_swap_process,
} = require('../../controllers/myaccount/simswap')

const {
	requireSignIn
} = require('../../controllers/auth');

router.post("/myaccount/simswap/webmyaccount_validate_iccid_info",
			requireSignIn, 
			webmyaccount_validate_iccid_info);
router.post("/myaccount/simswap/website_myaccount_sim_swap_process",
			requireSignIn, 
			website_myaccount_sim_swap_process);

module.exports = router;