const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    web_myaccount_view_chargeback_info,
    web_myaccount_chargeback_unsuspend,
    web_myaccount_check_card_no,
    web_myaccount_validate_voucher,
    web_myaccount_burn_voucher
} = require('../../controllers/myaccount/chargeback')

const {
	requireSignIn
} = require('../../controllers/auth');

router.post("/myaccount/chargeback/web_myaccount_view_chargeback_info",
			requireSignIn, 
			web_myaccount_view_chargeback_info);
router.post("/myaccount/chargeback/web_myaccount_chargeback_unsuspend",
			requireSignIn, 
			web_myaccount_chargeback_unsuspend);
router.post("/myaccount/chargeback/web_myaccount_check_card_no",
			requireSignIn, 
			web_myaccount_check_card_no);
router.post("/myaccount/chargeback/web_myaccount_validate_voucher",
			requireSignIn, 
			web_myaccount_validate_voucher);			
router.post("/myaccount/chargeback/web_myaccount_burn_voucher",
			requireSignIn, 
			web_myaccount_burn_voucher);

module.exports = router;