const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	create_sim_entry_log
} = require('../controllers/sim_entry_log')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/sim_entry_log/create_sim_entry_log",requireSignIn, create_sim_entry_log)

module.exports = router;