const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	get_store_search_info
} = require('../controllers/store_locator')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/store_locator/get_store_search_info",requireSignIn, get_store_search_info)

module.exports = router;