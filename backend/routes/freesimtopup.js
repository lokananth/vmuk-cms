const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	freesim_Order ,
	referral_order_new_sim,
	create_sim_with_credit,
	threeds_payment_log_insert,
	freesim_with_bundle_topup
} = require('../controllers/freesimtopup');
const {
	requireSignIn
} = require('../controllers/auth');

// create application/json parser
//var jsonParser = bodyParser.json()

router.post("/freesimtopup/freesim_Order", requireSignIn, freesim_Order);
router.post("/freesimtopup/referral_order_new_sim", requireSignIn,referral_order_new_sim);
router.post("/freesimtopup/create_sim_with_credit", requireSignIn, create_sim_with_credit)
router.post("/freesimtopup/threeds_payment_log_insert", requireSignIn, threeds_payment_log_insert)
router.post("/freesimtopup/freesim_with_bundle_topup", requireSignIn, freesim_with_bundle_topup)

module.exports = router;