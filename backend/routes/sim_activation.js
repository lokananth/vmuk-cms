const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	activation_sim_register_check,
	activation_insert_new_susbscriber,
	activation_insertfreesim_registered
} = require('../controllers/sim_activation')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/sim_activation/activation_sim_register_check",requireSignIn,  activation_sim_register_check)
router.post("/sim_activation/activation_insert_new_susbscriber",requireSignIn,  activation_insert_new_susbscriber)
router.post("/sim_activation/activation_insertfreesim_registered", requireSignIn, activation_insertfreesim_registered)

module.exports = router;