const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    Websitecentralllomcountrylist,
    Websitecentralllomrates,
    Websitecentralllomgetarealist,
    Websitecentralllomgetareacode,
    Websitecentralllomgetavailablenumber,
    Websitecentralllomreleaseavailableno,
    Websitecentralcheckmultinbsubscribtion,
    Websitecentralllomsubscribe,
} = require('../controllers/llom')

// create application/json parser
//var jsonParser = bodyParser.json()

router.post("/llom/Websitecentralllomcountrylist", Websitecentralllomcountrylist);
router.post("/llom/Websitecentralllomrates", Websitecentralllomrates);
router.post("/llom/Websitecentralllomgetarealist", Websitecentralllomgetarealist);
router.post("/llom/Websitecentralllomgetareacode", Websitecentralllomgetareacode);
router.post("/llom/Websitecentralllomreleaseavailableno", Websitecentralllomreleaseavailableno);
router.post("/llom/Websitecentralcheckmultinbsubscribtion", Websitecentralcheckmultinbsubscribtion);
router.post("/llom/Websitecentralllomsubscribe", Websitecentralllomsubscribe);

module.exports = router;