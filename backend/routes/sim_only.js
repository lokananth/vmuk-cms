const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	website_central_paymonthly_get_plan,
	website_central_paymonthly_insertdirectdebitorder,
	website_central_paymonthly_write_log,
	website_central_paymonthly_insertnewsubscriber,
	website_central_paymonthly_update_existsubscriber,
	website_central_paymonthly_freesim_create,
	website_central_paymonthly_getitembyitemid,
	website_central_paymonthly_insert_ddaccount,
	website_central_postpaid_subscribe,
	website_central_pp_addneworder_detail,
	paym_create_gocard_mandate_account,
	website_central_paym_plan_by_mobileno,
	website_central_paym_change_plan_request

} = require('../controllers/sim_only')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/sim_only/website_central_paymonthly_get_plan",requireSignIn, website_central_paymonthly_get_plan);
router.post("/sim_only/website_central_paymonthly_insertdirectdebitorder", requireSignIn,website_central_paymonthly_insertdirectdebitorder);
router.post("/sim_only/website_central_paymonthly_write_log", requireSignIn,website_central_paymonthly_write_log);
router.post("/sim_only/website_central_paymonthly_insertnewsubscriber",requireSignIn, website_central_paymonthly_insertnewsubscriber);
router.post("/sim_only/website_central_paymonthly_update_existsubscriber", requireSignIn,website_central_paymonthly_update_existsubscriber);
router.post("/sim_only/website_central_paymonthly_freesim_create", requireSignIn,website_central_paymonthly_freesim_create);
router.post("/sim_only/website_central_paymonthly_getitembyitemid", requireSignIn,website_central_paymonthly_getitembyitemid);
router.post("/sim_only/website_central_paymonthly_insert_ddaccount",requireSignIn, website_central_paymonthly_insert_ddaccount);
router.post("/sim_only/website_central_postpaid_subscribe", requireSignIn,website_central_postpaid_subscribe);
router.post("/sim_only/website_central_pp_addneworder_detail", requireSignIn,website_central_pp_addneworder_detail);
router.post("/sim_only/paym_create_gocard_mandate_account", requireSignIn,paym_create_gocard_mandate_account);
router.post("/sim_only/website_central_paym_plan_by_mobileno", requireSignIn,website_central_paym_plan_by_mobileno);
router.post("/sim_only/website_central_paym_change_plan_request", requireSignIn,website_central_paym_change_plan_request);



module.exports = router;


