const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	top_up_amount,
	top_up_active,
	top_up_process_ccdc_sms,
	top_up_payment_failed_trans,
	top_up_auto_enable,
	top_up_auto_log,
	top_up_log_details,
	top_up_auto_disable,
	top_up_auto_payment_type_register
} = require('../controllers/topup');
const {
	requireSignIn
} = require('../controllers/auth');

// create application/json parser
//var jsonParser = bodyParser.json()

router.post("/topup/top_up_amount", requireSignIn, top_up_amount);
router.post("/topup/top_up_active", requireSignIn,top_up_active);
router.post("/topup/top_up_process_ccdc_sms", requireSignIn, top_up_process_ccdc_sms)
router.post("/topup/top_up_payment_failed_trans", requireSignIn, top_up_payment_failed_trans)
router.post("/topup/top_up_auto_enable", requireSignIn, top_up_auto_enable)
router.post("/topup/top_up_auto_log", requireSignIn, top_up_auto_log)
router.post("/topup/top_up_log_details", requireSignIn,top_up_log_details )
router.post("/topup/top_up_auto_disable", requireSignIn,top_up_auto_disable )
router.post("/topup/top_up_auto_payment_type_register", requireSignIn,top_up_auto_payment_type_register )

module.exports = router;