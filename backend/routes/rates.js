const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    Websitecentralstandardsmartrates,
    Websitecentralvectoneappnational,
    Websitecentralgetcallingcodedetail,
    Websitecentralgetroamingratesinfo,
    Websitecentralvectoneapprates,
    Websitecentralvectonehappyrates,
    Websitecentralratenational,
    Websitecentralcheaprates
} = require('../controllers/rates');
const {
	requireSignIn
} = require('../controllers/auth');

// create application/json parser
//var jsonParser = bodyParser.json()

router.post("/rates/Websitecentralstandardsmartrates", requireSignIn, Websitecentralstandardsmartrates);
router.post("/rates/Websitecentralvectoneappnational", requireSignIn, Websitecentralvectoneappnational);
router.post("/rates/Websitecentralgetcallingcodedetail", requireSignIn, Websitecentralgetcallingcodedetail);
router.post("/rates/Websitecentralgetroamingratesinfo", requireSignIn, Websitecentralgetroamingratesinfo);
router.post("/rates/Websitecentralvectoneapprates", requireSignIn, Websitecentralvectoneapprates);
router.post("/rates/Websitecentralvectonehappyrates", requireSignIn, Websitecentralvectonehappyrates);
router.post("/rates/Websitecentralratenational", requireSignIn, Websitecentralratenational);
router.post("/rates/Websitecentralcheaprates", requireSignIn, Websitecentralcheaprates);


module.exports = router;