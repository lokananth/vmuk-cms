const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	get_countryname_code,
	get_operator_name,
	getmobilenamecode,
	getmobilemodelcode,
	getmobilediscursion,
	getdataactivation
} = require('../controllers/features')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/features/get_countryname_code",requireSignIn,  get_countryname_code)
router.post("/features/get_operator_name",requireSignIn, get_operator_name)
router.post("/features/getmobilenamecode",requireSignIn,  getmobilenamecode)
router.post("/features/getmobilemodelcode",requireSignIn,  getmobilemodelcode)
router.post("/features/getmobilediscursion",requireSignIn,  getmobilediscursion)
router.post("/features/getdataactivation",requireSignIn,  getdataactivation)

module.exports = router;