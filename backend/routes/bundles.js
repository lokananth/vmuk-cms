const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	bundles_ppc_customer_info,
	bundles_list,
	website_freesim_update_customer_info,
	website_central_view_bundle_category,
	website_central_view_special_country,
	website_central_view_bundle_min_info,
	website_central_bundle_subscribe,
	website_central_tigo_bundle_log,
	website_central_check_eligiblity_freesim_bundle_topup,
	web_myaccount_check_bundle_eligibility,
	website_record_special_bundle_success,
	website_central_subscribe_tigo_number,
	website_central_tigo_get_region_info,
	website_central_tigo_shop_location_info,
	website_central_tigo_shop_direction_info,
	tigo_senegal_ussd_bundle_subscribe,
	website_central_conference_bundle_subscribe,
	website_central_freesim_bundle_eligiblity_check
} = require('../controllers/bundles')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/bundles/bundles_ppc_customer_info",requireSignIn, bundles_ppc_customer_info);
router.post("/bundles/bundles_list",requireSignIn,  bundles_list);
router.post("/bundles/website_freesim_update_customer_info",requireSignIn,  website_freesim_update_customer_info);
router.post("/bundles/website_central_view_bundle_category",requireSignIn,  website_central_view_bundle_category);
router.post("/bundles/website_central_view_special_country",requireSignIn,  website_central_view_special_country);
router.post("/bundles/website_central_view_bundle_min_info",requireSignIn,  website_central_view_bundle_min_info);
router.post("/bundles/website_central_bundle_subscribe", requireSignIn, website_central_bundle_subscribe);
router.post("/bundles/website_central_tigo_bundle_log",requireSignIn,  website_central_tigo_bundle_log);
router.post("/bundles/website_central_check_eligiblity_freesim_bundle_topup",requireSignIn,  website_central_check_eligiblity_freesim_bundle_topup);
router.post("/bundles/web_myaccount_check_bundle_eligibility",requireSignIn,  web_myaccount_check_bundle_eligibility);
router.post("/bundles/website_record_special_bundle_success",requireSignIn,  website_record_special_bundle_success);
router.post("/bundles/website_central_subscribe_tigo_number",requireSignIn,  website_central_subscribe_tigo_number);
router.post("/bundles/website_central_tigo_get_region_info",requireSignIn,  website_central_tigo_get_region_info);
router.post("/bundles/website_central_tigo_shop_location_info",requireSignIn,  website_central_tigo_shop_location_info);
router.post("/bundles/website_central_tigo_shop_direction_info",requireSignIn,  website_central_tigo_shop_direction_info);
router.post("/bundles/tigo_senegal_ussd_bundle_subscribe",requireSignIn,  tigo_senegal_ussd_bundle_subscribe);
router.post("/bundles/website_central_conference_bundle_subscribe",requireSignIn,  website_central_conference_bundle_subscribe);
router.post("/bundles/website_central_freesim_bundle_eligiblity_check", requireSignIn, website_central_freesim_bundle_eligiblity_check);


module.exports = router;


