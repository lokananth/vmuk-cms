const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
    Websitecentralforgotpasswordbymobile,    
    Websitecentralforgotpasswdexpcountinsert,    
    Websitecentralforgotpasswdcheckexpirycount,    
    Websitecentralresetpassword,  
    Signin,
    Websitecentralregistercheckmsisdn,   
    Websitecentralregisterpersonalinfo,   
    Websitecentralemailcheckverification,   
    Websitecentralemailverifyupdate,   
} = require('../controllers/myaccount')
const {
	requireSignIn
} = require('../controllers/auth');
// create application/json parser
//var jsonParser = bodyParser.json()

router.post("/myaccount/Websitecentralforgotpasswordbymobile",requireSignIn, Websitecentralforgotpasswordbymobile);
router.post("/myaccount/Websitecentralforgotpasswdexpcountinsert",requireSignIn, Websitecentralforgotpasswdexpcountinsert);
router.post("/myaccount/Websitecentralforgotpasswdcheckexpirycount",requireSignIn, Websitecentralforgotpasswdcheckexpirycount);
router.post("/myaccount/Websitecentralresetpassword",requireSignIn, Websitecentralresetpassword);
router.post("/myaccount/Signin",requireSignIn, Signin);
router.post("/myaccount/Websitecentralregistercheckmsisdn",requireSignIn, Websitecentralregistercheckmsisdn);
router.post("/myaccount/Websitecentralregisterpersonalinfo",requireSignIn, Websitecentralregisterpersonalinfo);
router.post("/myaccount/Websitecentralemailcheckverification",requireSignIn, Websitecentralemailcheckverification);
router.post("/myaccount/Websitecentralemailverifyupdate",requireSignIn, Websitecentralemailverifyupdate);


module.exports = router;