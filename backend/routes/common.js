const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	wp_get_wp_retailer_type,
	website_central_service_add_check,
	wp_maintain_website_retailersp,
	website_central_rfr_referral_topup,
	website_central_update_email_delivery,
	website_insert_customer_contact_info,
	website_central_insert_email_log
} = require('../controllers/common')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/common/wp_get_wp_retailer_type",requireSignIn, wp_get_wp_retailer_type);
router.post("/common/website_central_service_add_check",requireSignIn, website_central_service_add_check);
router.post("/common/wp_maintain_website_retailersp",requireSignIn, wp_maintain_website_retailersp);
router.post("/common/website_central_rfr_referral_topup",requireSignIn, website_central_rfr_referral_topup);
router.post("/common/website_central_update_email_delivery",requireSignIn, website_central_update_email_delivery);
router.post("/common/website_insert_customer_contact_info",requireSignIn, website_insert_customer_contact_info);
router.post("/common/website_central_insert_email_log",requireSignIn, website_central_insert_email_log);




module.exports = router;


