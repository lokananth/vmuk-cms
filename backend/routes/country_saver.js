const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	get_registration_info,
	get_idx,
	check_balance,
	checkmsisdn_addons_info,
	insert_registration,
	update_registration,
	update_registration_pay_with_creditcard,
	insert_payment_failed_transaction,
	update_registration_pay_with_creditcard_failed,
	addon_registration,
	insert_activity_log,
	hutch_bundle_subscribe
} = require('../controllers/country_saver')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/country_saver/get_registration_info",requireSignIn, get_registration_info)
router.post("/country_saver/get_idx",requireSignIn, get_idx)
router.post("/country_saver/check_balance",requireSignIn, check_balance)
router.post("/country_saver/checkmsisdn_addons_info",requireSignIn, checkmsisdn_addons_info)
router.post("/country_saver/insert_registration",requireSignIn, insert_registration)
router.post("/country_saver/update_registration",requireSignIn, update_registration)
router.post("/country_saver/update_registration_pay_with_creditcard",requireSignIn, update_registration_pay_with_creditcard)
router.post("/country_saver/insert_payment_failed_transaction",requireSignIn, insert_payment_failed_transaction )
router.post("/country_saver/update_registration_pay_with_creditcard_failed",requireSignIn, update_registration_pay_with_creditcard_failed)
router.post("/country_saver/addon_registration",requireSignIn, addon_registration)
router.post("/country_saver/insert_activity_log", requireSignIn,insert_activity_log)
router.post("/country_saver/hutch_bundle_subscribe", requireSignIn,hutch_bundle_subscribe)

module.exports = router;