const express = require('express')
var bodyParser = require('body-parser')
const router = express.Router()
const  { 
	simorder_get_price,
	simorder_check_address,
	simorder_subsid_create,
	freesim_create
} = require('../controllers/send_sim_abroad')

const {
	requireSignIn
} = require('../controllers/auth');

router.post("/send_sim_abroad/simorder_get_price",requireSignIn,  simorder_get_price)
router.post("/send_sim_abroad/simorder_check_address", requireSignIn, simorder_check_address)
router.post("/send_sim_abroad/simorder_subsid_create", requireSignIn, simorder_subsid_create)
router.post("/send_sim_abroad/freesim_create", requireSignIn, freesim_create)

module.exports = router;