var dbconfig = {
        user:process.env.DB_USER,
        password:process.env.DB_PASSWORD,
        server: process.env.DB_SERVER, // You can use 'localhost\\instance' to connect to named instance
        database:process.env.DB_NAME,
        port: parseInt(process.env.DB_PORT),
        encrypt: false
    }
module.exports = dbconfig;