const express = require('express')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
require('dotenv').config()
// import routes
const authRoutes = require('./routes/auth');
const topupRoutes = require('./routes/topup');
const ratesRoutes = require('./routes/rates');
const bundleRoutes = require('./routes/bundles');
const freesimtopupRoutes = require('./routes/freesimtopup');
const myaccountRoutes = require('./routes/myaccount');
const simOnlyRoutes = require('./routes/sim_only');
const countrysaveerRoutes = require('./routes/country_saver');
const featureRoutes = require('./routes/features');
const sendsimabroadRoutes = require('./routes/send_sim_abroad');
const simactivationRoutes = require('./routes/sim_activation');
const simentrylogRoutes = require('./routes/sim_entry_log');
const storelocatorRoutes = require('./routes/store_locator');
const commonRoutes = require('./routes/common');
const llomRoutes = require('./routes/llom');
//My Account Modules
const chargebackRoutes = require('./routes/myaccount/chargeback');
const simswapRoutes = require('./routes/myaccount/simswap');
const referafriendRoutes = require('./routes/myaccount/referafriend');
const topupBundles = require('./routes/myaccount/topupBundles');
const account = require('./routes/myaccount/account');
const notifications = require('./routes/myaccount/notifications');
const proof_of_usage = require('./routes/myaccount/proof_of_usage');
const users = require('./routes/myaccount/users');
const portal = require('./routes/myaccount/portal');

//APP
const app = express();

//D
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
//routes middleware

app.get('/', function (req, res) {
  
});

app.use("/api",authRoutes);
app.use("/api",topupRoutes);
app.use("/api",ratesRoutes);
app.use("/api",bundleRoutes);
app.use("/api",freesimtopupRoutes);
app.use("/api",myaccountRoutes);
app.use("/api",simOnlyRoutes);
app.use("/api",countrysaveerRoutes);
app.use("/api",featureRoutes);
app.use("/api",sendsimabroadRoutes);
app.use("/api",simactivationRoutes);
app.use("/api",simentrylogRoutes);
app.use("/api",storelocatorRoutes);
app.use("/api",commonRoutes);
app.use("/api",llomRoutes);
//MyAccount Modules
app.use("/api",chargebackRoutes);
app.use("/api",simswapRoutes);
app.use("/api",referafriendRoutes)
app.use("/api",topupBundles)
app.use("/api",notifications)
app.use("/api",account)
app.use("/api",proof_of_usage)
app.use("/api",users)
app.use("/api",portal)

const port = process.env.PORT || 5000

app.listen(port, () => {
	console.log(`Server is running on port ${port}`)
})