const notifications = require('../../models/myaccount/notifications');


exports.web_update_personal_details_myaccount = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["mobileno"] = req.body.mobileno;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["address3"] = req.body.address3;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["countrycode"] = req.body.countrycode;
	post_input["email"] = req.body.email;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["landlinenumber"] = req.body.landlinenumber;
	post_input["language"] = req.body.language;
	post_input["gender"] = req.body.gender;
	post_input["bySMS"] = req.body.bySMS;
	post_input["byEmail"] = req.body.byEmail;
	post_input["birthDate"] = req.body.birthDate;
	post_input["houseno"] = req.body.houseno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["byCall"] = req.body.byCall;
	post_input["securityanswer"] = req.body.securityanswer;


	const result = await notifications.web_update_personal_details_myaccount(post_input);
	res.json( { result } );
}



