const referafriend = require('../../models/myaccount/referafriend');

exports.web_rfr_transaction_list_myaccount = async function (req,res,next)  { 
	var post_input = new Object;
	post_input["user_id"] = req.body.user_id;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await referafriend.web_rfr_transaction_list_myaccount(post_input);
	res.json( { result } );
}

exports.web_rfr_avail_credit_info_myaccount = async function (req,res,next)  { 
	var post_input = new Object;
	post_input["user_id"] = req.body.user_id;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await referafriend.web_rfr_avail_credit_info_myaccount(post_input);
	res.json( { result } );
}

exports.web_get_list_subscribed_bundle_myaccount = async function (req,res,next)  { 
	var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await referafriend.web_get_list_subscribed_bundle_myaccount(post_input);
	res.json( { result } );
}