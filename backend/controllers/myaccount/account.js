const account = require('../../models/myaccount/account');


exports.get_call_history = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["Msisdn"] = req.body.Msisdn;
	post_input["DateFrom"] = req.body.DateFrom;
	post_input["DateTo"] = req.body.DateTo;
	post_input["Type"] = req.body.Type;
	post_input["SiteCode"] = req.body.SiteCode;
	post_input["count"] = req.body.count;

	const result = await account.get_call_history(post_input);
	res.json( { result } );
}
exports.get_payment_history = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["startdate"] = req.body.startdate;
	post_input["enddate"] = req.body.enddate;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["count"] = req.body.count;


	const result = await account.get_payment_history(post_input);
	res.json( { result } );
}
exports.Webgetmaimeidtl = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;


	const result = await account.Webgetmaimeidtl(post_input);
	res.json( { result } );
}
exports.get_brand_type_by_mobileno = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;


	const result = await account.get_brand_type_by_mobileno(post_input);
	res.json( { result } );
}
exports.get_subscribed_info = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;


	const result = await account.get_subscribed_info(post_input);
	res.json( { result } );
}
exports.get_paym_plan_id = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["brand"] = req.body.brand;


	const result = await account.get_paym_plan_id(post_input);
	res.json( { result } );
}

exports.get_myaccount_subsrcibed_bundle = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["date_from"] = req.body.date_from;
	post_input["date_to"] = req.body.date_to;
	post_input["payment_type"] = req.body.payment_type;
	post_input["plan"] = req.body.plan;


	const result = await account.get_myaccount_subsrcibed_bundle(post_input);
	res.json( { result } );
}
exports.personal_login_myaccount = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["passwd"] = req.body.passwd;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;

	const result = await account.personal_login_myaccount(post_input);
	res.json( { result } );
}

