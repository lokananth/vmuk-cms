const portal = require('../../models/myaccount/portal');


exports.webmyaccountcheckcardno = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["last6digit_cc_no"] = req.body.last6digit_cc_no;


	const result = await portal.webmyaccountcheckcardno(post_input);
	res.json( { result } );
}


exports.GetCCUpdates = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;

	const result = await portal.GetCCUpdates(post_input);
	res.json( { result } );
}

exports.web_get_personal_details_myaccount = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;

	const result = await portal.web_get_personal_details_myaccount(post_input);
	res.json( { result } );
}

exports.GetCcardStatus = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["Mobileno"] = req.body.Mobileno;
	post_input["Sitecode"] = req.body.Sitecode;

	const result = await portal.GetCcardStatus(post_input);
	res.json( { result } );
}
exports.InsertCCUpdates = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileNo"] = req.body.mobileNo;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["ccNumber"] = req.body.ccNumber;
	post_input["houseNo"] = req.body.houseNo;
	post_input["streetName"] = req.body.streetName;
	post_input["city"] = req.body.city;
	post_input["country"] = req.body.country;
	post_input["createdby"] = req.body.createdby;
	post_input["postcode"] = req.body.postcode;

	const result = await portal.InsertCCUpdates(post_input);
	res.json( { result } );
}
exports.GetRegisterNumberList = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["Mainmobileno"] = req.body.Mainmobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;

	const result = await portal.GetRegisterNumberList(post_input);
	res.json( { result } );
}
exports.GetOutstandingAmount = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;

	const result = await portal.GetOutstandingAmount(post_input);
	res.json( { result } );
}
exports.GetRenewalInfo = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;

	const result = await portal.GetRenewalInfo(post_input);
	res.json( { result } );
}
exports.GetBundlePackBalance = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	
	post_input["brand"] = req.body.brand;

	const result = await portal.GetBundlePackBalance(post_input);
	res.json( { result } );
}

