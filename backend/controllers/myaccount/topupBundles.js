const topupBundles = require('../../models/myaccount/topupBundles');


exports.TopupProcessByCredidCard = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["paymentid"] = req.body.paymentid;
	post_input["paymentref"] = req.body.paymentref;
	post_input["amount"] = req.body.amount;
	post_input["ccdc_curr"] = req.body.ccdc_curr;
	post_input["calledby"] = req.body.calledby;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await topupBundles.TopupProcessByCredidCard(post_input);
	res.json( { result } );
}

exports.SendOnlySMS = async function (req,res,next)  {
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["message"] = req.body.message;
	post_input["scenerio"] = req.body.scenerio;

	const result = await topupBundles.SendOnlySMS(post_input);
	res.json( { result } );
}

exports.SubscribeDmukBundle = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["usertype"] = req.body.usertype;
	post_input["userinfo"] = req.body.userinfo;
	post_input["bundleid"] = req.body.bundleid;
	post_input["paymode"] = req.body.paymode;
	post_input["processby"] = req.body.processby;
	post_input["payment_ref"] = req.body.payment_ref;
	post_input["bundle_price"] = req.body.bundle_price;



	const result = await topupBundles.SubscribeDmukBundle(post_input);
	res.json( { result } );
}

exports.GetDMUKBundleinfo = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["Group_id"] = req.body.Group_id;

	const result = await topupBundles.SubscribeDmukBundle(post_input);
	res.json( { result } );
}

exports.GetBundleInfoDetails = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["group_id"] = req.body.group_id;

	const result = await topupBundles.GetBundleInfoDetails(post_input);
	res.json( { result } );
}
exports.GetmyaccountCheckBundleEligibility = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["bundleid"] = req.body.bundleid;

	const result = await topupBundles.GetmyaccountCheckBundleEligibility(post_input);
	res.json( { result } );
}
exports.Web_TopUpAmount_SP_myaccount = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;


	const result = await topupBundles.Web_TopUpAmount_SP_myaccount(post_input);
	res.json( { result } );
}
exports.DeleteCCUpdates = async function (req,res,next)  {
    var post_input = new Object;
	post_input["mobileNo"] = req.body.mobileNo;

	const result = await topupBundles.DeleteCCUpdates(post_input);
	res.json( { result } );
}

exports.TopupExisting = async function (req,res,next)  {
    var post_input = new Object;
	post_input["siteCode"] = req.body.siteCode;
	post_input["appCode"] = req.body.appCode;
	post_input["ProductCode"] = req.body.ProductCode;
	post_input["paymentagent"] = req.body.paymentagent;
	post_input["servicetype"] = req.body.servicetype;
	post_input["paymentmode"] = req.body.paymentmode;
	post_input["accountid"] = req.body.accountid;
	post_input["ccno"] = req.body.ccno;
	post_input["cvv"] = req.body.cvv;
	post_input["amount"] = req.body.amount;
	post_input["currency"] = req.body.currency;
	post_input["IpAddress"] = req.body.IpAddress;
	post_input["mobileno"] = req.body.mobileno;
	post_input["autoTopup"] = req.body.autoTopup;
	post_input["productid"] = req.body.productid;
	post_input["brand"] = req.body.brand;

	const result = await topupBundles.TopupExisting(post_input);
	res.json( { result } );
}
exports.GetFreeSimOrderProcessStep2 = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["mobile_no"] = req.body.mobile_no;
	post_input["credit_card_number"] = req.body.credit_card_number;
	post_input["topup_amount"] = req.body.topup_amount;


	const result = await topupBundles.GetFreeSimOrderProcessStep2(post_input);
	res.json( { result } );
}
exports.AddNewOrder = async function (req,res,next)  {
    var post_input = new Object;
	post_input["email"] = req.body.email;
	post_input["itemId"] = req.body.itemId;
	post_input["orderKey"] = req.body.orderKey;
	post_input["orderID"] = req.body.orderID;
	post_input["orderRef"] = req.body.orderRef;
	post_input["Sitecode"] = req.body.Sitecode;


	const result = await topupBundles.AddNewOrder(post_input);
	res.json( { result } );
}

exports.UpdateAutoTopupStatus = async function (req,res,next)  {
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["Brand"] = req.body.Brand;
	post_input["Status"] = req.body.Status;



	const result = await topupBundles.UpdateAutoTopupStatus(post_input);
	res.json( { result } );
}
exports.webMyaccountEnableAutoTopup = async function (req,res,next)  {
    var post_input = new Object;
	post_input["MobileNo"] = req.body.MobileNo;
	post_input["SubscriptionID"] = req.body.SubscriptionID;
	post_input["TopupAmount"] = req.body.TopupAmount;
	post_input["TopupCurr"] = req.body.TopupCurr;
	post_input["LastOrderID"] = req.body.LastOrderID;
	post_input["MinLevelID"] = req.body.MinLevelID;
	post_input["ProductID"] = req.body.ProductID;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["calledby"] = req.body.calledby;
	post_input["payment_ref"] = req.body.payment_ref;

	const result = await topupBundles.webMyaccountEnableAutoTopup(post_input);
	res.json( { result } );
}
exports.Websitemyaccountcancelautorenew = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["bundleid"] = req.body.bundleid;
	post_input["processby"] = req.body.processby;
	post_input["enable_auto_renew"] = req.body.enable_auto_renew;


	const result = await topupBundles.Websitemyaccountcancelautorenew(post_input);
	res.json( { result } );
}
