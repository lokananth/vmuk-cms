const proof_of_usage = require('../../models/myaccount/proof_of_usage');


exports.get_call_history = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["Msisdn"] = req.body.Msisdn;
	post_input["DateFrom"] = req.body.DateFrom;
	post_input["DateTo"] = req.body.DateTo;
	post_input["Type"] = req.body.Type;
	post_input["SiteCode"] = req.body.SiteCode;
	post_input["count"] = req.body.count;

	const result = await proof_of_usage.get_call_history(post_input);
	res.json( { result } );
}

exports.Webgetmaimeidtl = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;


	const result = await proof_of_usage.Webgetmaimeidtl(post_input);
	res.json( { result } );
}
