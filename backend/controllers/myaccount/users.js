const users = require('../../models/myaccount/users');


exports.get_brand_type_by_mobileno = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;


	const result = await users.get_brand_type_by_mobileno(post_input);
	res.json( { result } );
}


exports.personal_login_myaccount = async function (req,res,next)  {  
    var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["passwd"] = req.body.passwd;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;

	const result = await users.personal_login_myaccount(post_input);
	res.json( { result } );
}

