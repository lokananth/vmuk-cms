const chargeback = require('../../models/myaccount/chargeback');


exports.web_myaccount_view_chargeback_info = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	const result = await chargeback.web_myaccount_view_chargeback_info(post_input);
	res.json( { result } );
}

exports.web_myaccount_chargeback_unsuspend = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["paid_amount"] = req.body.paid_amount;
	post_input["paid_reference"] = req.body.paid_reference;
	post_input["calledby"] = req.body.calledby;
	post_input["amount_master"] = req.body.amount_master;
	const result = await chargeback.web_myaccount_chargeback_unsuspend(post_input);
	res.json( { result } );
}

exports.web_myaccount_check_card_no = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["last6digit_cc_no"] = req.body.last6digit_cc_no;
	const result = await chargeback.web_myaccount_check_card_no(post_input);
	res.json( { result } );
}

exports.web_myaccount_validate_voucher = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["pin"] = req.body.pin;
	const result = await chargeback.web_myaccount_validate_voucher(post_input);
	res.json( { result } );
}

exports.web_myaccount_burn_voucher = async function (req,res,next)  {
    var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["voucher_id"] = req.body.voucher_id;
	post_input["amount"] = req.body.amount;
	post_input["called_by"] = req.body.called_by;
	const result = await chargeback.web_myaccount_burn_voucher(post_input);
	res.json( { result } );
}