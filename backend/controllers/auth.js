const auth = require('../models/auth');
const jwt = require('jsonwebtoken');
require('dotenv').config()

exports.auth_login = async function (req,res)  {
	/*
	var post_input = new Object;
	post_input["id"] = req.body.id;
	post_input["username"] = req.body.username;
	post_input["email"] = req.body.email;
	const user = await auth.auth_login(post_input);
	*/
	const user = {
		"id": process.env.JWT_ID,
	    "username": process.env.JWT_USERNAME,
	    "email": process.env.JWT_EMAIL
	}
	if(user != undefined) {
		jwt.sign({user}, process.env.JWT_SECRET, {expiresIn: '900s'}, (err,token) =>{
			res.json( { 
				token
			});
		});	
	}
}

exports.requireSignIn = (req,res,next) => {
	//get auth header value
	const bearerHeader = req.headers['authorization'];
	// check if bearer is undefined.
	if(typeof bearerHeader !== 'undefined') {
		// split at the space
		const bearer = bearerHeader.split(' ');
		//Get token from array
		const bearerToken = bearer[1];
		// set the token
		req.token = bearerToken;
		//Next middleware
		jwt.verify(req.token, process.env.JWT_SECRET , (err, authData) => {
			if(err) {
				res.sendStatus(403);
			}
			else {
				next();
			}
		})
		
	} else {
		res.sendStatus(403);
	}
}

