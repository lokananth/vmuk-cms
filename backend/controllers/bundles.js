const bundles = require('../models/bundles');


exports.bundles_ppc_customer_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["freesimid"] = req.body.freesimid;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await bundles.bundles_ppc_customer_info(post_input);
	res.json( { result } );
	
}


exports.bundles_list = async function (req,res,next)  {

	var post_input = new Object;
	post_input["cateogry_id"] = req.body.cateogry_id;
	post_input["product_name"] = req.body.product_name;
	post_input["sitecode"] = req.body.sitecode;
	post_input["special_bundle_flag"] = req.body.special_bundle_flag;
	post_input["addon_bundle_id"] = req.body.addon_bundle_id;
	post_input["lang_code"] = req.body.lang_code;
	const result = await bundles.bundles_list(post_input);
	res.json( { result } );
	
}

exports.website_freesim_update_customer_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["freesimid"] = req.body.freesimid;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["email"] = req.body.email;
	post_input["postcode"] = req.body.postcode;
	post_input["houseno"] = req.body.houseno;
	

	const result = await bundles.website_freesim_update_customer_info(post_input);
	
	res.json( { result } );
	
}


exports.website_central_view_bundle_category = async function (req,res,next)  {

	var post_input = new Object;
	post_input["cateogry_id"] = req.body.cateogry_id;
	post_input["product_code"] = req.body.product_code;
	post_input["sitecode"] = req.body.sitecode;
	post_input["special_bundle_flag"] = req.body.special_bundle_flag;
	post_input["addon_bundle_id"] = req.body.addon_bundle_id;
	post_input["lang_code"] = req.body.lang_code;
	const result = await bundles.bundles_list(post_input);
	res.json( { result } );
	
}

exports.website_central_view_special_country = async function (req,res,next)  {

	var post_input = new Object;
	post_input["product_code"] = req.body.cateogry_id;
	post_input["sitecode"] = req.body.product_code;
	post_input["config_id"] = req.body.config_id;

	const result = await bundles.website_central_view_bundle_category(post_input);
	
	res.json( { result } );
	
}

exports.website_central_view_special_country = async function (req,res,next)  {

	var post_input = new Object;
	post_input["product_code"] = req.body.cateogry_id;
	post_input["sitecode"] = req.body.product_code;
	post_input["config_id"] = req.body.config_id;

	const result = await bundles.website_central_view_special_country(post_input);
	
	res.json( { result } );
	
}

exports.website_central_view_bundle_min_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["config_id"] = req.body.config_id;

	const result = await bundles.website_central_view_bundle_min_info(post_input);
	
	res.json( { result } );
	
}


exports.website_central_bundle_subscribe = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["usertype"] = req.body.usertype;
	post_input["userinfo"] = req.body.userinfo;
	post_input["bundleid"] = req.body.bundleid;
	post_input["paymode"] = req.body.paymode;
	post_input["process_by"] = req.body.process_by;
	post_input["payment_ref"] = req.body.payment_ref;
	post_input["promo_flag"] = req.body.promo_flag;
	post_input["promo_code"] = req.body.promo_code;
	post_input["bundle_price"] = req.body.bundle_price;
	post_input["dest_number"] = req.body.dest_number;
	post_input["bundle_type"] = req.body.bundle_type;
	post_input["purchase_url"] = req.body.purchase_url;
	

	const result = await bundles.website_central_bundle_subscribe(post_input);
	
	res.json( { result } );
	
}

exports.website_central_tigo_bundle_log = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["mobileno"] = req.body.mobileno;
	post_input["bundleid"] = req.body.bundleid;
	post_input["paymode"] = req.body.paymode;
	post_input["process_by"] = req.body.process_by;
	post_input["payment_ref"] = req.body.payment_ref;

	

	const result = await bundles.website_central_tigo_bundle_log(post_input);
	
	res.json( { result } );
	
}
exports.website_central_check_eligiblity_freesim_bundle_topup = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["promo_code"] = req.body.promo_code;


	

	const result = await bundles.website_central_check_eligiblity_freesim_bundle_topup(post_input);
	
	res.json( { result } );
	
}

exports.web_myaccount_check_bundle_eligibility = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["mobileno"] = req.body.mobileno;
	post_input["bundleid"] = req.body.bundleid;


	

	const result = await bundles.web_myaccount_check_bundle_eligibility(post_input);
	
	res.json( { result } );
	
}

exports.website_record_special_bundle_success = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["mobileno"] = req.body.mobileno;
	post_input["paymodee"] = req.body.paymodee;
	post_input["process_by"] = req.body.process_by;
	post_input["payment_ref"] = req.body.payment_ref;
	post_input["bundle_price"] = req.body.bundle_price;
	post_input["bundleid"] = req.body.bundleid;


	

	const result = await bundles.website_record_special_bundle_success(post_input);
	
	res.json( { result } );
	
}

exports.website_central_subscribe_tigo_number = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["mobileno"] = req.body.mobileno;
	post_input["bundleid"] = req.body.bundleid;
	post_input["first_name"] = req.body.first_name;
	post_input["last_name"] = req.body.last_name;
	post_input["city"] = req.body.city;
	post_input["contact_no"] = req.body.contact_no;
	post_input["pickup_location"] = req.body.pickup_location;
	post_input["paymode"] = req.body.paymode;
	post_input["payment_reference"] = req.body.payment_reference;
	post_input["tigo_countrycode"] = req.body.tigo_countrycode;
	post_input["price"] = req.body.price;
	post_input["location_id"] = req.body.location_id;
	post_input["ussd_prefix"] = req.body.ussd_prefix;
	


	

	const result = await bundles.website_central_subscribe_tigo_number(post_input);
	
	res.json( { result } );
	
}

exports.website_central_tigo_get_region_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["tigo_country_code"] = req.body.tigo_country_code;

	const result = await bundles.website_central_tigo_get_region_info(post_input);
	
	res.json( { result } );
	
}

exports.website_central_tigo_shop_location_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["region_id"] = req.body.region_id;

	const result = await bundles.website_central_tigo_shop_location_info(post_input);
	
	res.json( { result } );
	
}

exports.website_central_tigo_shop_direction_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["region_id"] = req.body.region_id;
	post_input["location_id"] = req.body.location_id;

	const result = await bundles.website_central_tigo_shop_direction_info(post_input);
	
	res.json( { result } );
	
}

exports.tigo_senegal_ussd_bundle_subscribe = async function (req,res,next)  {

	var post_input = new Object;
	post_input["mundio_cli"] = req.body.mundio_cli;
	post_input["tigo_cli"] = req.body.tigo_cli;
	post_input["ussd_prefix"] = req.body.ussd_prefix;
	post_input["paymode"] = req.body.paymode;
	post_input["processby"] = req.body.processby;

	const result = await bundles.tigo_senegal_ussd_bundle_subscribe(post_input);
	
	res.json( { result } );
	
}

exports.website_central_conference_bundle_subscribe = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["usertype"] = req.body.usertype;
	post_input["userinfo"] = req.body.userinfo;
	post_input["bundleid"] = req.body.bundleid;
	post_input["paymode"] = req.body.paymode;
	post_input["process_by"] = req.body.process_by;
	post_input["payment_ref"] = req.body.payment_ref;
	post_input["bundle_price"] = req.body.bundle_price;
	post_input["dest_number"] = req.body.dest_number;


	const result = await bundles.website_central_conference_bundle_subscribe(post_input);
	
	res.json( { result } );
	
}

exports.website_central_freesim_bundle_eligiblity_check = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["cc_number"] = req.body.cc_number;
	post_input["ip_address"] = req.body.ip_address;



	const result = await bundles.website_central_freesim_bundle_eligiblity_check(post_input);
	
	res.json( { result } );
	
}