const features = require('../models/features');


exports.get_countryname_code = async function (req,res,next)  {
	var post_input = new Object;
	post_input["projectCode"] = req.body.projectCode;
	post_input["languageCode"] = req.body.languageCode;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await features.get_countryname_code(post_input);
	res.json( { result } );
}

exports.get_operator_name = async function (req,res,next)  {
	var post_input = new Object;
	post_input["countryCode"] = req.body.countryCode;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await features.get_operator_name(post_input);
	res.json( { result } );
}

exports.getmobilenamecode = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await features.getmobilenamecode(post_input);
	res.json( { result } );
}

exports.getmobilemodelcode = async function (req,res,next)  {
	var post_input = new Object;
	post_input["projectCode"] = req.body.projectCode;
	post_input["languageCode"] = req.body.languageCode;
	post_input["Mobilecode"] = req.body.Mobilecode;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await features.getmobilemodelcode(post_input);
	res.json( { result } );
}

exports.getmobilediscursion = async function (req,res,next)  {
	var post_input = new Object;
	post_input["projectCode"] = req.body.projectCode;
	post_input["languageCode"] = req.body.languageCode;
	post_input["MobileID"] = req.body.MobileID;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await features.getmobilediscursion(post_input);
	res.json( { result } );
}

exports.getdataactivation = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["data_activation"] = req.body.data_activation;
	post_input["msisdn"] = req.body.msisdn;
	const result = await features.getdataactivation(post_input);
	res.json( { result } );
}
