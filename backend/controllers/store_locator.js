const storelocator = require('../models/store_locator');


exports.get_store_search_info = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["search_option"] = req.body.search_option;
	post_input["search_value"] = req.body.search_value;
	const result = await storelocator.get_store_search_info(post_input);
	res.json( { result } );
}
