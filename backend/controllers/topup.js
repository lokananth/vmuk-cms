const topup = require('../models/topup');


exports.top_up_amount = async function (req,res,next)  {
	//console.log(topup.top_up_amount())
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["website"] = req.body.website;
	const result = await topup.top_up_amount(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.top_up_active = async function (req,res,next)  {
	var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
    post_input["brand"] = req.body.brand;
    post_input["sitecode"] = req.body.sitecode;
    post_input["device_type"] = req.body.device_type;
    post_input["browser"] = req.body.browser;
    post_input["os_version"] = req.body.os_version;
    post_input["topup_url"] = req.body.topup_url;
    post_input["processtype"] = req.body.processtype;
	const result = await topup.top_up_active(post_input);
	res.json( { result } );
}

exports.top_up_process_ccdc_sms = async function (req,res,next)  {
	var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
    post_input["paymentid"] = req.body.paymentid;
    post_input["paymentref"] = req.body.paymentref;
    post_input["productid"] = req.body.productid;
    post_input["amount"] = req.body.amount;
    post_input["ccdc_curr"] = req.body.ccdc_curr;
    post_input["calledby"] = req.body.calledby;
    post_input["sitecode"] = req.body.sitecode;
    post_input["brand"] = req.body.brand;
    post_input["topup_url"] = req.body.topup_url;
	const result = await topup.top_up_process_ccdc_sms(post_input);
	res.json( { result } );
}

exports.top_up_payment_failed_trans = async function (req,res,next)  {
	var post_input = new Object;
	post_input["pay_reference"] = req.body.pay_reference;
    post_input["amount"] = req.body.amount;
    post_input["pay_date"] = req.body.pay_date;
    post_input["mobileno"] = req.body.mobileno;
    post_input["Reason"] = req.body.Reason;
    post_input["Description"] = req.body.Description;
    post_input["sitecode"] = req.body.sitecode;
    post_input["brand"] = req.body.brand;
	const result = await topup.top_up_payment_failed_trans(post_input);
	res.json( { result } );
}

exports.top_up_auto_enable = async function (req,res,next)  {
	var post_input = new Object;
	post_input["MobileNo"] = req.body.MobileNo;
	post_input["SubscriptionID"] = req.body.SubscriptionID;
	post_input["TopupAmount"] = req.body.TopupAmount;
	post_input["TopupCurr"] = req.body.TopupCurr;
	post_input["LastOrderID"] = req.body.LastOrderID;
	post_input["MinLevelID"] = req.body.MinLevelID
	post_input["ProductID"] = req.body.ProductID;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["calledby"] = req.body.calledby;
	post_input["payment_ref"] = req.body.payment_ref;
	const result = await topup.top_up_auto_enable(post_input);
	res.json( { result } );
}

exports.top_up_auto_log = async function (req,res,next)  {
	var post_input = new Object;
 	post_input["MobileNo"] = req.body.MobileNo;
    post_input["SubscriptionID"] = req.body.SubscriptionID;
    post_input["MinLevelID"] = req.body.MinLevelID;
    post_input["ItemID"] = req.body.ItemID;
    post_input["OrderID"] = req.body.OrderID;
    post_input["reason"] = req.body.reason;
    post_input["result"] = req.body.result;
    post_input["reasonsubs"] = req.body.reasonsubs; 
    post_input["description"] = req.body.description; 
    post_input["sitecode"] = req.body.sitecode; 
    post_input["brand"] = req.body.brand; 
	const result = await topup.top_up_auto_enable(post_input);
	res.json( { result } );
}

exports.top_up_log_details = async function (req,res,next)  {
	var post_input = new Object;
 	post_input["mobileno"] = req.body.mobileno;
    post_input["paymentRef"] = req.body.paymentRef;
    post_input["sitecode"] = req.body.sitecode;
    post_input["brand"] = req.body.brand; 
	const result = await topup.top_up_log_details(post_input);
	res.json( { result } );
}

exports.top_up_auto_disable = async function (req,res,next)  {
	var post_input = new Object;
 	post_input["MobileNo"] = req.body.MobileNo;
    post_input["calledby"] = req.body.calledby;
    post_input["brand"] = req.body.brand;
    post_input["sitecode"] = req.body.sitecode; 
	const result = await topup.top_up_auto_disable(post_input);
	res.json( { result } );
}

exports.top_up_auto_payment_type_register = async function (req,res,next)  {
	var post_input = new Object;
 	post_input["msisdn"] = req.body.msisdn;
    post_input["topup_type"] = req.body.topup_type;
    post_input["processby"] = req.body.processby;
    post_input["lastccnum"] = req.body.lastccnum; 
    post_input["brand"] = req.body.brand;
    post_input["sitecode"] = req.body.sitecode
	const result = await topup.top_up_auto_payment_type_register(post_input);
	res.json( { result } );
}


