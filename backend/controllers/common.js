const common = require('../models/common');



exports.wp_get_wp_retailer_type = async function (req,res,next)  {

	var post_input = new Object;

	const result = await common.wp_get_wp_retailer_type(post_input);
	
	res.json( { result } );
	
}

exports.website_central_service_add_check = async function (req,res,next)  {

	var post_input = new Object;
	post_input["reference_id"] = req.body.reference_id;
	post_input["svc_type"] = req.body.svc_type;
	post_input["sitecode"] = req.body.sitecode;
	const result = await common.website_central_service_add_check(post_input);
	
	res.json( { result } );
	
}

exports.wp_maintain_website_retailersp = async function (req,res,next)  {

	var post_input = new Object;
	post_input["Retailer_Name"] = req.body.Retailer_Name;
	post_input["Email"] = req.body.Email;
	post_input["Country"] = req.body.Country;
	post_input["RT_Address1"] = req.body.RT_Address1;
	post_input["RT_Address2"] = req.body.RT_Address2;
	post_input["Post_Code"] = req.body.Post_Code;
	post_input["Prefered_Lang"] = req.body.Prefered_Lang;
	post_input["Mobile_No"] = req.body.Mobile_No;
	post_input["Landline_No"] = req.body.Landline_No;
	post_input["WR_University_Name"] = req.body.WR_University_Name;
	post_input["RT_Brand_Type"] = req.body.RT_Brand_Type;
	const result = await common.wp_maintain_website_retailersp(post_input);
	
	res.json( { result } );
	
}


exports.website_central_rfr_referral_topup = async function (req,res,next)  {

	var post_input = new Object;
	post_input["mobile_no"] = req.body.mobile_no;
	post_input["topup_amount"] = req.body.topup_amount;
	post_input["credit_card_number"] = req.body.credit_card_number;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	const result = await common.website_central_rfr_referral_topup(post_input);
	
	res.json( { result } );
	
}
exports.website_central_update_email_delivery = async function (req,res,next)  {

	var post_input = new Object;
	post_input["mobile_no"] = req.body.mobile_no;
	post_input["scenerio"] = req.body.scenerio;
	post_input["email_status"] = req.body.email_status;
	post_input["date"] = req.body.date;
	post_input["processby"] = req.body.processby;
	const result = await common.website_central_update_email_delivery(post_input);
	
	res.json( { result } );
	
}
exports.website_insert_customer_contact_info = async function (req,res,next)  {

	var post_input = new Object;
	post_input["Sitecode"] = req.body.Sitecode;
	post_input["First_name"] = req.body.First_name;
	post_input["Last_name"] = req.body.Last_name;
	post_input["Email"] = req.body.Email;
	post_input["Vectone_mobileno"] = req.body.Vectone_mobileno;
	post_input["Alternate_contactno"] = req.body.Alternate_contactno;
	post_input["Department"] = req.body.Department;
	post_input["Subject"] = req.body.Subject;
	post_input["Message"] = req.body.Message;
	const result = await common.website_insert_customer_contact_info(post_input);
	
	res.json( { result } );
	
}
exports.website_central_insert_email_log = async function (req,res,next)  {

	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["email_status"] = req.body.email_status;
	post_input["freesimid"] = req.body.freesimid;
	post_input["firstname"] = req.body.firstname;
	post_input["email"] = req.body.email;
	post_input["scenerio"] = req.body.scenerio;
	post_input["secnerio_type"] = req.body.secnerio_type;
	post_input["mobileno"] = req.body.mobileno;
	post_input["search_date"] = req.body.search_date;
	post_input["email_sent_by"] = req.body.email_sent_by;
	post_input["email_template"] = req.body.email_template;
	const result = await common.website_central_insert_email_log(post_input);
	
	res.json( { result } );
	
}