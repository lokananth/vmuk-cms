const sendsimabroad = require('../models/send_sim_abroad');


exports.simorder_get_price = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["Country_Code"] = req.body.Country_Code;
	const result = await sendsimabroad.simorder_get_price(post_input);
	res.json( { result } );
}

exports.simorder_check_address = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["houseno"] = req.body.houseno;
	post_input["postcode"] = req.body.postcode;
	const result = await sendsimabroad.simorder_check_address(post_input);
	res.json( { result } );
}

exports.simorder_subsid_create = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;
	post_input["conversion_source"] = req.body.conversion_source;
	post_input["country_source"] = req.body.country_source;
	post_input["channel_source"] = req.body.channel_source;
	post_input["simtype"] = req.body.simtype;
	const result = await sendsimabroad.simorder_subsid_create(post_input);
	res.json( { result } );
}

exports.freesim_create = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["freesimstatus"] = req.body.freesimstatus;
	post_input["sourceaddress"] = req.body.sourceaddress;
	post_input["statid"] = req.body.statid;
	post_input["ordersimurl"] = req.body.ordersimurl;
	post_input["referrer"] = req.body.referrer;
	post_input["ip_address"] = req.body.ip_address;
	post_input["cybersourceid"] = req.body.cybersourceid;
	const result = await sendsimabroad.freesim_create(post_input);
	res.json( { result } );
}
