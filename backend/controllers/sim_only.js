const sim_only = require('../models/sim_only');


exports.website_central_paymonthly_get_plan = async function (req,res,next)  {

	var post_input = new Object;
	post_input["bundleid"] = req.body.bundleid;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	const result = await sim_only.website_central_paymonthly_get_plan(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_paymonthly_insertdirectdebitorder = async function (req,res,next)  {

	var post_input = new Object;
	post_input["orderid"] = req.body.orderid;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["originatorID"] = req.body.originatorID;
	post_input["buildingsociety"] = req.body.buildingsociety;
	post_input["accountname"] = req.body.accountname;
	post_input["branchsortcode"] = req.body.branchsortcode;
	post_input["accountnumber"] = req.body.accountnumber;
	post_input["ddref"] = req.body.ddref;
	const result = await sim_only.website_central_paymonthly_insertdirectdebitorder(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_paymonthly_write_log = async function (req,res,next)  {

	var post_input = new Object;
	post_input["reference_id"] = req.body.reference_id;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["subscriber_type"] = req.body.subscriber_type;
	post_input["activity"] = req.body.activity;

	const result = await sim_only.website_central_paymonthly_write_log(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_paymonthly_insertnewsubscriber = async function (req,res,next)  {

	var post_input = new Object;
	post_input["title"] = req.body.title;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;
	post_input["id_type"] = req.body.id_type;
	post_input["id_number"] = req.body.id_number;
	post_input["doc_exp_Date"] = req.body.doc_exp_Date;

	const result = await sim_only.website_central_paymonthly_insertnewsubscriber(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.website_central_paymonthly_update_existsubscriber = async function (req,res,next)  {

	var post_input = new Object;
	post_input["title"] = req.body.title;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;


	const result = await sim_only.website_central_paymonthly_update_existsubscriber(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_paymonthly_freesim_create = async function (req,res,next)  {

	var post_input = new Object;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["freesimstatus"] = req.body.freesimstatus;
	post_input["cybersourceid"] = req.body.cybersourceid;
	post_input["postpaidinfo"] = req.body.postpaidinfo;
	post_input["iccid"] = req.body.iccid;
	post_input["loginname"] = req.body.loginname;
	post_input["pac"] = req.body.pac;
	post_input["existingno"] = req.body.existingno;
	post_input["promocode"] = req.body.promocode;
	post_input["url"] = req.body.url;
	post_input["ip_address"] = req.body.ip_address;
	post_input["itemid"] = req.body.itemid;
	post_input["currency_Symbol"] = req.body.currency_Symbol;
	post_input["source_address"] = req.body.source_address;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["is_bill_gap"] = req.body.is_bill_gap;
	


	const result = await sim_only.website_central_paymonthly_freesim_create(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}


exports.website_central_paymonthly_getitembyitemid = async function (req,res,next)  {

	var post_input = new Object;
	post_input["itemid"] = req.body.itemid;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;


	const result = await sim_only.website_central_paymonthly_getitembyitemid(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_paymonthly_insert_ddaccount = async function (req,res,next)  {

	var post_input = new Object;
	post_input["paymentref"] = req.body.paymentref;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["sort_code"] = req.body.sort_code;
	post_input["account_num"] = req.body.account_num;
	post_input["account_name"] = req.body.account_name;
	post_input["branch_code"] = req.body.branch_code;
	post_input["first_name"] = req.body.first_name;
	post_input["country"] = req.body.country;
	post_input["amount"] = req.body.amount;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["freesimid"] = req.body.freesimid;


	const result = await sim_only.website_central_paymonthly_insert_ddaccount(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_postpaid_subscribe = async function (req,res,next)  {

	var post_input = new Object;
	post_input["usertype"] = req.body.usertype;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["userinfo"] = req.body.userinfo;
	post_input["is_postpaid"] = req.body.is_postpaid;
	post_input["credinit"] = req.body.credinit;
	post_input["new_trffclass"] = req.body.new_trffclass;
	post_input["renew_delay"] = req.body.renew_delay;
	post_input["renew_mode"] = req.body.renew_mode;
	post_input["renew_datemode"] = req.body.renew_datemode;
	post_input["renew_chosendate"] = req.body.renew_chosendate;
	post_input["bundleid"] = req.body.bundleid;
	post_input["processby"] = req.body.processby;


	const result = await sim_only.website_central_postpaid_subscribe(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}

exports.website_central_pp_addneworder_detail = async function (req,res,next)  {

	var post_input = new Object;

	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	post_input["email"] = req.body.email;
	post_input["itemId"] = req.body.itemId;
	post_input["orderID"] = req.body.orderID;
	post_input["orderRef"] = req.body.orderRef;
	post_input["orderKey"] = req.body.orderKey;
	post_input["status"] = req.body.status;
	post_input["process_description"] = req.body.process_description;



	const result = await sim_only.website_central_pp_addneworder_detail(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.paym_create_gocard_mandate_account = async function (req,res,next)  {

	var post_input = new Object;

	post_input["customerid"] = req.body.customerid;
	post_input["mobileno"] = req.body.mobileno;
	post_input["mandate_id"] = req.body.mandate_id;
	post_input["isvalid"] = req.body.isvalid;
	post_input["gocard_customerid"] = req.body.gocard_customerid;
	post_input["mandate_status"] = req.body.mandate_status;




	const result = await sim_only.paym_create_gocard_mandate_account(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.website_central_paym_plan_by_mobileno = async function (req,res,next)  {

	var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["product_name"] = req.body.product_name;
	post_input["lang_code"] = req.body.lang_code;
	post_input["plan_upgrade"] = req.body.plan_upgrade;
	post_input["brand"] = req.body.brand;
	post_input["sitecode"] = req.body.sitecode;
	const result = await sim_only.website_central_paym_plan_by_mobileno(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.website_central_paym_change_plan_request = async function (req,res,next)  {

	var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["prevplan_end_date"] = req.body.prevplan_end_date;
	post_input["newplan"] = req.body.newplan;
	post_input["oldplan"] = req.body.oldplan;
	post_input["newplan_start_date"] = req.body.newplan_start_date;
	post_input["bundleid"] = req.body.bundleid;
	post_input["pay_reference"] = req.body.pay_reference;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["change_type"] = req.body.change_type;
	post_input["processby"] = req.body.processby;
	
	const result = await sim_only.website_central_paym_change_plan_request(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
