const simentrylog = require('../models/sim_entry_log');


exports.create_sim_entry_log = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["email"] = req.body.email;
	post_input["simorder_type"] = req.body.simorder_type;
	post_input["ip_address"] = req.body.ip_address;
	post_input["device_type"] = req.body.device_type;
	post_input["browser_type"] = req.body.browser_type;
	post_input["url"] = req.body.url;
	const result = await simentrylog.create_sim_entry_log(post_input);
	res.json( { result } );
}
