const freesimtopup = require('../models/freesimtopup');


exports.freesim_Order = async function (req,res,next)  {
	var post_input = new Object;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;
	post_input["conversion_source"] = req.body.conversion_source;
	post_input["country_source"] = req.body.country_source;
	post_input["channel_source"] = req.body.channel_source;
	post_input["freesimstatus"] = req.body.freesimstatus;
	post_input["sourceaddress"] = req.body.sourceaddress;
	post_input["statid"] = req.body.statid;
	post_input["ordersimurl"] = req.body.ordersimurl;
	post_input["simtype"] = req.body.simtype;
	post_input["nb_of_sim"] = req.body.nb_of_sim;
	post_input["ip_address"] = req.body.ip_address;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["fav_call_country"] = req.body.fav_call_country;
	post_input["landing_page_url"] = req.body.landing_page_url;
	post_input["last_vist_page_url"] = req.body.last_vist_page_url;
	post_input["old_freesimid"] = req.body.old_freesimid;
	post_input["promo_bundleid"] = req.body.promo_bundleid;
	post_input["cybersourceid"] = req.body.cybersourceid;
	const result = await freesimtopup.freesim_Order(post_input);
	res.json( { result } );
}

exports.referral_order_new_sim = async function (req,res,next)  {
	var post_input = new Object;
	post_input["nb_of_sim"] = req.body.nb_of_sim;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;
	post_input["conversion_source"] = req.body.conversion_source;
	post_input["country_source"] = req.body.country_source;
	post_input["channel_source"] = req.body.channel_source;
	post_input["freesimstatus"] = req.body.freesimstatus;
	post_input["sourceaddress"] = req.body.sourceaddress;
	post_input["statid"] = req.body.statid;
	post_input["ordersimurl"] = req.body.ordersimurl;
	post_input["referrer"] = req.body.referrer;
	post_input["ReferralType"] = req.body.ReferralType;
	post_input["clickid"] = req.body.clickid;
	post_input["ip_address"] = req.body.ip_address;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await freesimtopup.referral_order_new_sim(post_input);
	res.json( { result } );
}

exports.create_sim_with_credit = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;
	post_input["conversion_source"] = req.body.conversion_source;
	post_input["country_source"] = req.body.country_source;
	post_input["channel_source"] = req.body.channel_source;
	post_input["simtype"] = req.body.simtype;
	post_input["freesimstatus"] = req.body.freesimstatus;
	post_input["freesimtype"] = req.body.freesimtype;
	post_input["cybersourceid"] = req.body.cybersourceid;
	post_input["ip_address"] = req.body.ip_address;
	post_input["ordersim_url"] = req.body.ordersim_url;
	post_input["source_address"] = req.body.source_address;
	const result = await freesimtopup.create_sim_with_credit(post_input);
	res.json( { result } );
}

exports.threeds_payment_log_insert = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["mobileno"] = req.body.mobileno;
	post_input["REFERENCE_ID"] = req.body.REFERENCE_ID;
	post_input["Pay_step"] = req.body.Pay_step;
	post_input["Pay_code"] = req.body.Pay_code;
	post_input["Pay_decision"] = req.body.Pay_decision;
	post_input["Pay_description"] = req.body.Pay_description;
	post_input["Pay_stage"] = req.body.Pay_stage;
	const result = await freesimtopup.threeds_payment_log_insert(post_input);
	res.json( { result } );
}

exports.freesim_with_bundle_topup = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["FreeSimId"] = req.body.FreeSimId;
	post_input["Ordered_Date"] = req.body.Ordered_Date;
	post_input["SubscriberId"] = req.body.SubscriberId;
	post_input["freesim_type"] = req.body.freesim_type;
	post_input["Bundle_Id"] = req.body.Bundle_Id;
	post_input["FreeSim_Status"] = req.body.FreeSim_Status;
	post_input["Payment_Ref"] = req.body.Payment_Ref;
	post_input["Topup_amt"] = req.body.Topup_amt;
	post_input["prodcutid"] = req.body.prodcutid;
	post_input["currency"] = req.body.currency;
	post_input["processby"] = req.body.processby;
	post_input["purchase_type"] = req.body.purchase_type;
	post_input["promo_code"] = req.body.promo_code;
	post_input["promo_flag"] = req.body.promo_flag;
	post_input["sim_price"] = req.body.sim_price;
	post_input["addon_bundle_id"] = req.body.addon_bundle_id;
	post_input["dest_number"] = req.body.dest_number;
	post_input["bundle_type"] = req.body.bundle_type;
	post_input["activation_flag"] = req.body.activation_flag;
	post_input["cc_number"] = req.body.cc_number;
	post_input["called_by"] = req.body.called_by;
	const result = await freesimtopup.freesim_with_bundle_topup(post_input);
	res.json( { result } );
}
