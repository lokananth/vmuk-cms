const countrysaver = require('../models/country_saver');


exports.get_registration_info = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["msisdn"] = req.body.msisdn;
	const result = await countrysaver.get_registration_info(post_input);
	res.json( { result } );
}

exports.get_idx = async function (req,res,next)  {
	var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["dest_msisdn"] = req.body.dest_msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.activation_insert_new_susbscriber(post_input);
	res.json( { result } );
}

exports.check_balance = async function (req,res,next)  {
	var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["price"] = req.body.price;
	post_input["reg_idx"] = req.body.reg_idx;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.check_balance(post_input);
	res.json( { result } );
}
exports.checkmsisdn_addons_info = async function (req,res,next)  {
	var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["dest_msisdn"] = req.body.dest_msisdn;
	post_input["product_type"] = req.body.product_type;
	post_input["productprice"] = req.body.productprice;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.checkmsisdn_addons_info(post_input);
	res.json( { result } );
}
exports.insert_registration = async function (req,res,next)  {
	var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["dest_msisdn"] = req.body.dest_msisdn;
	post_input["veri_code"] = req.body.veri_code;
	post_input["productid"] = req.body.productid;
	post_input["landline"] = req.body.landline;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.insert_registration(post_input);
	res.json( { result } );
}
exports.update_registration = async function (req,res,next)  {
	var post_input = new Object;
	post_input["destnumber"] = req.body.destnumber;
	post_input["vectnumber"] = req.body.vectnumber;
	post_input["status"] = req.body.status;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["verificationcode"] = req.body.verificationcode;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["email"] = req.body.email;
	post_input["postalcode"] = req.body.postalcode;
	post_input["houseno"] = req.body.houseno;
	post_input["street"] = req.body.street;
	post_input["city"] = req.body.city;
	post_input["country"] = req.body.country;
	post_input["titledest"] = req.body.titledest;
	post_input["firstnamedest"] = req.body.firstnamedest;
	post_input["lastnamedest"] = req.body.lastnamedest;
	post_input["postcodedest"] = req.body.postcodedest;
	post_input["housenumberdest"] = req.body.housenumberdest;
	post_input["streetdest"] = req.body.streetdest;
	post_input["towndest"] = req.body.towndest;
	post_input["countrydest"] = req.body.countrydest;
	post_input["reg_idx"] = req.body.reg_idx;
	post_input["amount"] = req.body.amount;
	post_input["landline"] = req.body.landline;
	post_input["checkaddonbundle"] = req.body.checkaddonbundle;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.update_registration(post_input);
	res.json( { result } );
}

exports.update_registration_pay_with_creditcard = async function (req,res,next)  {
	var post_input = new Object;
	post_input["destnumber"] = req.body.destnumber;
	post_input["vectnumber"] = req.body.vectnumber;
	post_input["status"] = req.body.status;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["verificationcode"] = req.body.verificationcode;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["email"] = req.body.email;
	post_input["postalcode"] = req.body.postalcode;
	post_input["houseno"] = req.body.houseno;
	post_input["street"] = req.body.street;
	post_input["city"] = req.body.city;
	post_input["country"] = req.body.country;
	post_input["titledest"] = req.body.titledest;
	post_input["firstnamedest"] = req.body.firstnamedest;
	post_input["lastnamedest"] = req.body.lastnamedest;
	post_input["postcodedest"] = req.body.postcodedest;
	post_input["housenumberdest"] = req.body.housenumberdest;
	post_input["streetdest"] = req.body.streetdest;
	post_input["towndest"] = req.body.towndest;
	post_input["countrydest"] = req.body.countrydest;
	post_input["reg_idx"] = req.body.reg_idx;
	post_input["amount"] = req.body.amount;
	post_input["landline"] = req.body.landline;
	post_input["checkaddonbundle"] = req.body.checkaddonbundle;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["creditcardid"] = req.body.creditcardid;
	post_input["sorderid"] = req.body.sorderid;
	const result = await countrysaver.update_registration_pay_with_creditcard(post_input);
	res.json( { result } );
}

exports.insert_payment_failed_transaction = async function (req,res,next)  {
	var post_input = new Object;
	post_input["pay_reference"] = req.body.pay_reference;
	post_input["amount"] = req.body.amount;
	post_input["pay_date"] = req.body.pay_date;
	post_input["mobileno"] = req.body.mobileno;
	post_input["Reason"] = req.body.Reason;
	post_input["Description"] = req.body.Description;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.insert_payment_failed_transaction(post_input);
	res.json( { result } );
}

exports.update_registration_pay_with_creditcard_failed = async function (req,res,next)  {
	var post_input = new Object;
	post_input["destnumber"] = req.body.destnumber;
	post_input["vectnumber"] = req.body.vectnumber;
	post_input["status"] = req.body.status;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["verificationcode"] = req.body.verificationcode;
	post_input["reg_idx"] = req.body.reg_idx;
	post_input["creditcardid"] = req.body.creditcardid;
	post_input["sorderid"] = req.body.sorderid;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.update_registration_pay_with_creditcard_failed(post_input);
	res.json( { result } );
}

exports.addon_registration = async function (req,res,next)  {
	var post_input = new Object;
	post_input["reg_idx"] = req.body.reg_idx;
	post_input["addon_msisdn"] = req.body.addon_msisdn;
	post_input["dest_number"] = req.body.dest_number;
	post_input["clitypes"] = req.body.clitypes;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.addon_registration(post_input);
	res.json( { result } );
}


exports.insert_activity_log = async function (req,res,next)  {
	var post_input = new Object;
	post_input["reg_idx"] = req.body.reg_idx;
	post_input["main_msisdn"] = req.body.main_msisdn;
	post_input["dest_msisdn"] = req.body.dest_msisdn;
	post_input["current_status"] = req.body.current_status;
	post_input["action"] = req.body.action;
	post_input["description"] = req.body.description;
	post_input["additional_info1"] = req.body.additional_info1;
	post_input["additional_info2"] = req.body.additional_info2;
	post_input["landline"] = req.body.landline;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await countrysaver.insert_activity_log(post_input);
	res.json( { result } );
}

exports.hutch_bundle_subscribe = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["msisdn"] = req.body.msisdn;
	post_input["dest_msisdn"] = req.body.dest_msisdn;
	post_input["bundleid"] = req.body.bundleid;
	post_input["processby"] = req.body.processby;
	const result = await countrysaver.hutch_bundle_subscribe(post_input);
	res.json( { result } );
}

