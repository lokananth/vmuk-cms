const simactivation = require('../models/sim_activation');


exports.activation_insertfreesim_registered = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["subscriberid"] = req.body.subscriberid;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sourceaddress"] = req.body.sourceaddress;
	post_input["ordersimurl"] = req.body.ordersimurl;
	post_input["referrer"] = req.body.referrer;
	const result = await simactivation.activation_insertfreesim_registered(post_input);
	res.json( { result } );
}

exports.activation_insert_new_susbscriber = async function (req,res,next)  {
	var post_input = new Object;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	post_input["title"] = req.body.title;
	post_input["firstname"] = req.body.firstname;
	post_input["lastname"] = req.body.lastname;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["state"] = req.body.state;
	post_input["countrycode"] = req.body.countrycode;
	post_input["telephone"] = req.body.telephone;
	post_input["mobilephone"] = req.body.mobilephone;
	post_input["fax"] = req.body.fax;
	post_input["email"] = req.body.email;
	post_input["birthdate"] = req.body.birthdate;
	post_input["question"] = req.body.question;
	post_input["answer"] = req.body.answer;
	post_input["subscribertype"] = req.body.subscribertype;
	post_input["sourcereg"] = req.body.sourcereg;
	post_input["subscriberchannel"] = req.body.subscriberchannel;
	post_input["personalidtype"] = req.body.personalidtype;
	post_input["personalidnumber"] = req.body.personalidnumber;
	post_input["proof_file_name"] = req.body.proof_file_name;
	const result = await simactivation.activation_insert_new_susbscriber(post_input);
	res.json( { result } );
}

exports.activation_sim_register_check = async function (req,res,next)  {
	var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	const result = await simactivation.activation_sim_register_check(post_input);
	res.json( { result } );
}

