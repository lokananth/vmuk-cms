const myaccount = require('../models/myaccount');


exports.Websitecentralforgotpasswordbymobile = async function (req,res,next)  {
    var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralforgotpasswordbymobile(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralforgotpasswdexpcountinsert = async function (req,res,next)  {
    var post_input = new Object;
	post_input["token"] = req.body.token;
	post_input["msisdn"] = req.body.msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralforgotpasswdexpcountinsert(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralforgotpasswdcheckexpirycount = async function (req,res,next)  {
    var post_input = new Object;
	post_input["token"] = req.body.token;
	post_input["msisdn"] = req.body.msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralforgotpasswdcheckexpirycount(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralresetpassword = async function (req,res,next)  {
    var post_input = new Object;
	post_input["password"] = req.body.password;
	post_input["msisdn"] = req.body.msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralresetpassword(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Signin = async function (req,res,next)  {
    var post_input = new Object;
	post_input["passwd"] = req.body.passwd;
	post_input["msisdn"] = req.body.msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Signin(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralregistercheckmsisdn = async function (req,res,next)  {
    var post_input = new Object;
	post_input["mobileno"] = req.body.mobileno;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralregistercheckmsisdn(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralregisterpersonalinfo = async function (req,res,next)  {
    var post_input = new Object;
	post_input["msisdn"] = req.body.msisdn;
	post_input["verifcode"] = req.body.verifcode;
	post_input["source_reg"] = req.body.source_reg;
	post_input["country"] = req.body.country;
	post_input["status"] = req.body.status;
	post_input["password"] = req.body.password;
	post_input["fname"] = req.body.fname;
	post_input["lname"] = req.body.lname;
	post_input["email"] = req.body.email;
	post_input["lang"] = req.body.lang;
	post_input["gender"] = req.body.gender;
	post_input["dob"] = req.body.dob;
	post_input["bysms"] = req.body.bysms;
	post_input["byemail"] = req.body.byemail;
	post_input["houseno"] = req.body.houseno;
	post_input["address1"] = req.body.address1;
	post_input["address2"] = req.body.address2;
	post_input["city"] = req.body.city;
	post_input["postcode"] = req.body.postcode;
	post_input["security_answer"] = req.body.security_answer;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralregisterpersonalinfo(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralemailcheckverification = async function (req,res,next)  {
    var post_input = new Object;
	post_input["email"] = req.body.email;
	post_input["msisdn"] = req.body.msisdn;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralemailcheckverification(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}
exports.Websitecentralemailverifyupdate = async function (req,res,next)  {
    var post_input = new Object;
	post_input["email"] = req.body.email;
	post_input["msisdn"] = req.body.msisdn;
	post_input["verify_code"] = req.body.verify_code;
	post_input["verify_flag"] = req.body.verify_flag;
	post_input["sitecode"] = req.body.sitecode;
	post_input["brand"] = req.body.brand;
	//console.log(post_input);
	const result = await myaccount.Websitecentralemailverifyupdate(post_input);
	//const result = "bearly";
	res.json( { result } );
	//console.log("test");
}