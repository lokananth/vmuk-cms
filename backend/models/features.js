var sql = require("mssql");
const dbconfig = require('../dbconfig');

const get_countryname_code = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('projectCode', sql.NVarChar(500), post_input["projectCode"]);
            request.input('languageCode', sql.NVarChar(500), post_input["languageCode"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_get_CountryName_code').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const get_operator_name = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('countryCode', sql.VarChar(5), post_input["countryCode"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_get_Operator_Name').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const getmobilenamecode = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_get_mobilename_code').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const getmobilemodelcode = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('projectCode', sql.NVarChar(500), post_input["projectCode"]);
            request.input('languageCode', sql.VarChar(500), post_input["languageCode"]);
            request.input('Mobilecode', sql.VarChar(500), post_input["Mobilecode"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_get_mobilemodel_code').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const getmobilediscursion = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('projectCode', sql.NVarChar(500), post_input["projectCode"]);
            request.input('languageCode', sql.NVarChar(500), post_input["languageCode"]);
            request.input('MobileID', sql.NVarChar(500), post_input["MobileID"]); 
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_get_mobile_discursion').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}
const getdataactivation = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('data_activation', sql.Int, post_input["data_activation"]);
            request.input('msisdn', sql.VarChar(500), post_input["msisdn"]);
            request.execute('website_central_get_sms_iwmsc_url').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

exports.get_countryname_code = get_countryname_code;
exports.get_operator_name = get_operator_name;
exports.getmobilenamecode = getmobilenamecode;
exports.getmobilemodelcode = getmobilemodelcode;
exports.getmobilediscursion = getmobilediscursion;
exports.getdataactivation = getdataactivation;
