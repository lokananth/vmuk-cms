var sql = require("mssql");
const dbconfig = require('../dbconfig');

const Websitecentralforgotpasswordbymobile = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_forgot_password_by_mobile').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralforgotpasswdexpcountinsert  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('token', sql.VarChar(100), post_input["token"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_forgot_passwd_expcount_insert').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralforgotpasswdcheckexpirycount  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('token', sql.VarChar(100), post_input["token"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_forgot_passwd_check_expiry_count').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralresetpassword  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('password', sql.VarChar(100), post_input["password"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_reset_password').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Signin  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('passwd', sql.VarChar(100), post_input["passwd"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_login_check').then (function(res, recordsets, returnValue, affected) {
                console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralregistercheckmsisdn  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobileno', sql.VarChar(30), post_input["mobileno"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_register_check_msisdn').then (function(res, recordsets, returnValue, affected) {
                console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralregisterpersonalinfo  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('verifcode', sql.VarChar(30), post_input["verifcode"]);
            request.input('source_reg', sql.VarChar(100), post_input["source_reg"]);
            request.input('country', sql.VarChar(30), post_input["country"]);
            request.input('status', sql.Int(11), post_input["status"]);
            request.input('password', sql.VarChar(30), post_input["password"]);
            request.input('fname', sql.VarChar(60), post_input["fname"]);
            request.input('lname', sql.VarChar(60), post_input["lname"]);
            request.input('email', sql.VarChar(100), post_input["email"]);
            request.input('lang', sql.VarChar(100), post_input["lang"]);
            request.input('gender', sql.VarChar(30), post_input["gender"]);
            request.input('dob', sql.VarChar(100), post_input["dob"]);
            request.input('bysms', sql.Int(11), post_input["bysms"]);
            request.input('byemail', sql.Int(11), post_input["byemail"]);
            request.input('houseno', sql.VarChar(30), post_input["houseno"]);
            request.input('address1', sql.VarChar(100), post_input["address1"]);
            request.input('address2', sql.VarChar(100), post_input["address2"]);
            request.input('city', sql.VarChar(100), post_input["city"]);
            request.input('postcode', sql.VarChar(20), post_input["postcode"]);
            request.input('security_answer', sql.VarChar(100), post_input["security_answer"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_register_personal_info').then (function(res, recordsets, returnValue, affected) {
                console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralemailcheckverification  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('email', sql.VarChar(100), post_input["email"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_email_check_verification').then (function(res, recordsets, returnValue, affected) {
                console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralemailverifyupdate  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(30), post_input["msisdn"]);
            request.input('email', sql.VarChar(100), post_input["email"]);
            request.input('verify_code', sql.VarChar(30), post_input["verify_code"]);
            request.input('verify_flag', sql.Int(11), post_input["verify_flag"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_email_verify_update').then (function(res, recordsets, returnValue, affected) {
                console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
exports.Websitecentralforgotpasswordbymobile  = Websitecentralforgotpasswordbymobile;
exports.Websitecentralforgotpasswdexpcountinsert  = Websitecentralforgotpasswdexpcountinsert;
exports.Websitecentralforgotpasswdcheckexpirycount  = Websitecentralforgotpasswdcheckexpirycount;
exports.Websitecentralresetpassword  = Websitecentralresetpassword;
exports.Signin  = Signin;
exports.Websitecentralregistercheckmsisdn  = Websitecentralregistercheckmsisdn;
exports.Websitecentralregisterpersonalinfo  = Websitecentralregisterpersonalinfo;
exports.Websitecentralemailcheckverification  = Websitecentralemailcheckverification;
exports.Websitecentralemailverifyupdate  = Websitecentralemailverifyupdate;
