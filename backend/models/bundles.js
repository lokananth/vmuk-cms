var sql = require("mssql");
const dbconfig = require('../dbconfig');

const bundles_ppc_customer_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('freesimid', sql.Int, post_input["freesimid"]);
            request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            
            //request.output('output_parameter', sql.Int);
            request.execute('website_freesim_customer_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const bundles_list = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('product_name', sql.VarChar(10), post_input["product_name"]);
            request.input('cateogry_id', sql.Int, post_input["cateogry_id"]);
            request.input('special_bundle_flag', sql.Int, post_input["special_bundle_flag"]);
            request.input('addon_bundle_id', sql.Int, post_input["addon_bundle_id"]);
            request.input('lang_code', sql.VarChar(3), 'en');
            
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_view_bundle_info_v2').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_freesim_update_customer_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('freesimid', sql.Int, post_input["freesimid"]);
            request.input('subscriberid', sql.Int, post_input["subscriberid"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('firstname', sql.VarChar(250), post_input["firstname"]);
            request.input('lastname', sql.VarChar(250), post_input["lastname"]);
            request.input('address1', sql.VarChar(200), post_input["address1"]);
            request.input('address2', sql.VarChar(200), post_input["address2"]);
            request.input('city', sql.VarChar(100), post_input["city"]);
            request.input('email', sql.VarChar(200), post_input["email"]);
            request.input('postcode', sql.VarChar(100), post_input["postcode"]);
            request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
            request.input('houseno', sql.VarChar(150), post_input["houseno"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_freesim_update_customer_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_view_bundle_category = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            
            request.input('product_code', sql.VarChar(10), post_input["product_code"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            
            
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_view_bundle_category').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_view_special_country = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            
            request.input('product_code', sql.VarChar(30), post_input["product_code"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('config_id', sql.Int, post_input["config_id"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_view_special_country').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_view_bundle_min_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            
            request.input('config_id', sql.Int, post_input["config_id"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_view_bundle_min_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_bundle_subscribe = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('brand', sql.Int, post_input["brand"]);
            request.input('usertype', sql.Int, post_input["usertype"]);
            request.input('userinfo', sql.VarChar(1000), post_input["userinfo"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('paymode', sql.Int, post_input["paymode"]);
            request.input('promo_flag', sql.Int, post_input["promo_flag"]);
            request.input('process_by', sql.VarChar(100), post_input["process_by"]);
            request.input('payment_ref', sql.VarChar(100), post_input["payment_ref"]);
            request.input('promo_code', sql.VarChar(100), post_input["promo_code"]);
            request.input('bundle_price', sql.Float, post_input["bundle_price"]);
            request.input('dest_number', sql.VarChar(100), post_input["dest_number"]);
            request.input('bundle_type', sql.Int, post_input["bundle_type"]);
            request.input('purchase_url', sql.VarChar(1000), post_input["purchase_url"]);
            

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_bundle_subscribe').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_central_tigo_bundle_log = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('brand', sql.Int, post_input["brand"]);
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('paymode', sql.Int, post_input["paymode"]);
            request.input('process_by', sql.VarChar(25), post_input["process_by"]);
            request.input('payment_ref', sql.VarChar(50), post_input["payment_ref"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_tigo_bundle_log').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_check_eligiblity_freesim_bundle_topup = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('brand', sql.Int, post_input["brand"]);
            request.input('promo_code', sql.VarChar(100), post_input["promo_code"]);
            
            

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_check_eligiblity_freesim_bundle_topup').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const web_myaccount_check_bundle_eligibility = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('mobileno', sql.VarChar(60), post_input["mobileno"]);
			request.input('brand', sql.Int, post_input["brand"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            
            

            //request.output('output_parameter', sql.Int);
            request.execute('WEB_myaccount_check_bundle_eligibility').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_record_special_bundle_success = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('paymode', sql.Int, post_input["paymode"]);
			request.input('process_by', sql.VarChar(25), post_input["process_by"]);
			request.input('payment_ref', sql.VarChar(50), post_input["payment_ref"]);
			request.input('bundle_price', sql.Float, post_input["bundle_price"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            
            //request.output('output_parameter', sql.Int);
            request.execute('website_record_special_bundle_success').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_subscribe_tigo_number = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
			request.input('brand', sql.Int, post_input["brand"]);
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('bundle_id', sql.Int, post_input["bundleid"]);
			request.input('first_name', sql.VarChar(100), post_input["first_name"]);
			request.input('last_name', sql.VarChar(100), post_input["last_name"]);
			request.input('city', sql.VarChar(30), post_input["city"]);
			request.input('contact_no', sql.VarChar(30), post_input["contact_no"]);
			request.input('pickup_location', sql.VarChar(250), post_input["pickup_location"]);
			request.input('paymode', sql.Int, post_input["paymode"]);			
            request.input('payment_reference', sql.VarChar(50), post_input["payment_reference"]);
			request.input('tigo_countrycode', sql.VarChar(20), post_input["tigo_countrycode"]);
			request.input('price', sql.Float, post_input["price"]);
			request.input('location_id', sql.Int, post_input["location_id"]);
			request.input('ussd_prefix', sql.VarChar(20), post_input["ussd_prefix"]);
            
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_subscribe_tigo_number').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_central_tigo_get_region_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('tigo_country_code', sql.VarChar(100), post_input["tigo_country_code"]);           
            

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_tigo_get_region_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_tigo_shop_location_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('region_id', sql.Int, post_input["region_id"]);           
            

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_tigo_shop_location_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_tigo_shop_direction_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('region_id', sql.Int, post_input["region_id"]);           
			request.input('location_id', sql.Int, post_input["location_id"]);           
            

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_tigo_shop_direction_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const tigo_senegal_ussd_bundle_subscribe = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('mundio_cli', sql.VarChar(20), post_input["mundio_cli"]);           
			request.input('tigo_cli', sql.VarChar(20), post_input["tigo_cli"]);           
			request.input('ussd_prefix', sql.VarChar(20), post_input["ussd_prefix"]);           
			request.input('paymode', sql.Int, post_input["paymode"]);           
			request.input('processby', sql.VarChar(50), post_input["processby"]);           
            

            //request.output('output_parameter', sql.Int);
            request.execute('tigo_senegal_ussd_bundle_subscribe').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_conference_bundle_subscribe = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('brand', sql.Int, post_input["brand"]);
			request.input('usertype', sql.Int, post_input["usertype"]);
			request.input('userinfo', sql.VarChar(60), post_input["userinfo"]);			
			request.input('bundleid', sql.Int, post_input["bundleid"]);           
			request.input('paymode', sql.Int, post_input["paymode"]);           
            request.input('process_by', sql.VarChar(60), post_input["process_by"]);
            request.input('payment_ref', sql.VarChar(60), post_input["payment_ref"]);
			request.input('bundle_price', sql.Float, post_input["bundle_price"]);
            request.input('dest_number', sql.VarChar(60), post_input["dest_number"]);

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_conference_bundle_subscribe').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_central_freesim_bundle_eligiblity_check = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('brand', sql.Int, post_input["brand"]);
			request.input('cc_number', sql.VarChar(60), post_input["cc_number"]);
			request.input('ip_address', sql.VarChar(60), post_input["ip_address"]);	         
            

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_freesim_bundle_eligiblity_check').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

exports.bundles_ppc_customer_info = bundles_ppc_customer_info;
exports.website_freesim_update_customer_info = website_freesim_update_customer_info;
exports.website_central_view_bundle_category = website_central_view_bundle_category;
exports.website_central_view_special_country = website_central_view_special_country;
exports.bundles_list = bundles_list;
exports.website_central_view_bundle_min_info = website_central_view_bundle_min_info;
exports.website_central_bundle_subscribe = website_central_bundle_subscribe;
exports.website_central_tigo_bundle_log = website_central_tigo_bundle_log;
exports.website_central_check_eligiblity_freesim_bundle_topup = website_central_check_eligiblity_freesim_bundle_topup;
exports.web_myaccount_check_bundle_eligibility = web_myaccount_check_bundle_eligibility;
exports.website_record_special_bundle_success = website_record_special_bundle_success;
exports.website_central_subscribe_tigo_number = website_central_subscribe_tigo_number;
exports.website_central_tigo_get_region_info = website_central_tigo_get_region_info;
exports.website_central_tigo_shop_location_info = website_central_tigo_shop_location_info;
exports.website_central_tigo_shop_direction_info = website_central_tigo_shop_direction_info;
exports.tigo_senegal_ussd_bundle_subscribe = tigo_senegal_ussd_bundle_subscribe;
exports.website_central_conference_bundle_subscribe = website_central_conference_bundle_subscribe;
exports.website_central_freesim_bundle_eligiblity_check = website_central_freesim_bundle_eligiblity_check;
