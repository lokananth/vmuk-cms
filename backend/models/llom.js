var sql = require("mssql");
const dbconfig = require('../dbconfig');

const Websitecentralllomcountrylist = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_countrylist').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralllomrates = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('name', sql.VarChar(90), post_input["name"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            request.input('paymode', sql.Int(11), post_input["paymode"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_rates').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralllomgetarealist = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('country_name', sql.VarChar(90), post_input["country_name"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_getAreaList').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralllomgetareacode = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('country_name', sql.VarChar(90), post_input["country_name"]);
            request.input('area_name', sql.VarChar(90), post_input["area_name"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_getAreacode').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralllomgetavailablenumber = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('country_name', sql.VarChar(90), post_input["country_name"]);
            request.input('area_name', sql.VarChar(90), post_input["area_name"]);
            request.input('area_code', sql.VarChar(90), post_input["area_code"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_getAvailableNumber').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralllomreleaseavailableno = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('number1', sql.VarChar(30), post_input["number1"]);
            request.input('number2', sql.VarChar(30), post_input["number2"]);
            request.input('number3', sql.VarChar(30), post_input["number3"]);
            request.input('number4', sql.VarChar(30), post_input["number4"]);
            request.input('number5', sql.VarChar(30), post_input["number5"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_release_availableno').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralcheckmultinbsubscribtion = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('accinfo', sql.VarChar(30), post_input["accinfo"]);
            request.input('svc_id', sql.Int(11), post_input["svc_id"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_check_multinb_subscribtion').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralllomsubscribe = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('accinfo', sql.VarChar(30), post_input["accinfo"]);
            request.input('acctype', sql.Int(11), post_input["acctype"]);
            request.input('svc_id', sql.Int(11), post_input["svc_id"]);
            request.input('input_regnumber', sql.VarChar(30), post_input["input_regnumber"]);
            request.input('called_by', sql.VarChar(30), post_input["called_by"]);
            request.input('processurl', sql.VarChar(30), post_input["processurl"]);
            request.input('payment_ref', sql.VarChar(30), post_input["payment_ref"]);            
            request.input('paymode', sql.Int(11), post_input["paymode"]);            
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_llom_subscribe').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
exports.Websitecentralllomcountrylist  = Websitecentralllomcountrylist ;
exports.Websitecentralllomrates  = Websitecentralllomrates ;
exports.Websitecentralllomgetarealist  = Websitecentralllomgetarealist;
exports.Websitecentralllomgetareacode  = Websitecentralllomgetareacode;
exports.Websitecentralllomgetavailablenumber  = Websitecentralllomgetavailablenumber;
exports.Websitecentralllomreleaseavailableno  = Websitecentralllomreleaseavailableno;
exports.Websitecentralcheckmultinbsubscribtion  = Websitecentralcheckmultinbsubscribtion;
exports.Websitecentralllomsubscribe  = Websitecentralllomsubscribe;
