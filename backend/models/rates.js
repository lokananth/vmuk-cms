var sql = require("mssql");
const dbconfig = require('../dbconfig');

const Websitecentralstandardsmartrates = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('Countrycode', sql.VarChar(30), post_input["Countrycode"]);
            request.input('product_code', sql.VarChar(30), post_input["product_code"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_standard_smart_rates').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralvectoneappnational = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('countrycode', sql.VarChar(30), post_input["countrycode"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_vectone_app_national').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralgetcallingcodedetail  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('product_code', sql.VarChar(30), post_input["product_code"]);
            request.input('country_code', sql.VarChar(30), post_input["country_code"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_get_callingcode_detail').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralgetroamingratesinfo  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('projectCode', sql.VarChar(30), post_input["projectCode"]);
            request.input('originCountryCode', sql.VarChar(30), post_input["originCountryCode"]);
            request.input('destCountryCode', sql.VarChar(30), post_input["destCountryCode"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_get_roaming_rates_info').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralvectoneapprates  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('countrycode', sql.VarChar(30), post_input["countrycode"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('website_sitecode', sql.VarChar(30), post_input["website_sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_vectone_app_rates').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralvectonehappyrates  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('countrycode', sql.VarChar(30), post_input["countrycode"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_vectone_happy_rates').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralratenational  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('countrycode', sql.VarChar(30), post_input["countrycode"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('brand', sql.Int(11), post_input["brand"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_rate_national').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Websitecentralcheaprates  = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('countrycode', sql.VarChar(30), post_input["countrycode"]);
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('product_code', sql.VarChar(30), post_input["product_code"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_cheap_rates').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
exports.Websitecentralstandardsmartrates = Websitecentralstandardsmartrates;
exports.Websitecentralvectoneappnational = Websitecentralvectoneappnational;
exports.Websitecentralgetcallingcodedetail  = Websitecentralgetcallingcodedetail ;
exports.Websitecentralgetroamingratesinfo  = Websitecentralgetroamingratesinfo ;
exports.Websitecentralvectoneapprates  = Websitecentralvectoneapprates ;
exports.Websitecentralvectonehappyrates  = Websitecentralvectonehappyrates ;
exports.Websitecentralratenational  = Websitecentralratenational ;
exports.Websitecentralcheaprates  = Websitecentralcheaprates ;
