var sql = require("mssql");
const dbconfig = require('../dbconfig');

const activation_insertfreesim_registered = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('subscriberid', sql.Int, post_input["subscriberid"]);
            request.input('mobileno', sql.VarChar(64), post_input["mobileno"]);
            request.input('sourceaddress', sql.VarChar(500), post_input["sourceaddress"]);
            request.input('ordersimurl', sql.VarChar(500), post_input["ordersimurl"]);
            request.input('referrer', sql.VarChar(255), post_input["referrer"]);
            request.execute('website_central_activation_InsertFreeSim_Registered').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const activation_insert_new_susbscriber = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('title', sql.VarChar(8), post_input["title"]);
            request.input('firstname', sql.VarChar(255), post_input["firstname"]);
            request.input('lastname', sql.VarChar(255), post_input["lastname"]);
            request.input('houseno', sql.VarChar(255), post_input["houseno"]);
            request.input('address1', sql.VarChar(255), post_input["address1"]);
            request.input('address2', sql.VarChar(255), post_input["address2"]);
            request.input('city', sql.VarChar(128), post_input["city"]);
            request.input('postcode', sql.VarChar(32), post_input["postcode"]);
            request.input('state', sql.VarChar(128), post_input["state"]);
            request.input('countrycode', sql.VarChar(32), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(64), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(64), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(64), post_input["fax"]);
            request.input('email', sql.VarChar(128), post_input["email"]);
            request.input('birthdate', sql.VarChar(128), post_input["birthdate"]);
            request.input('question', sql.VarChar(255), post_input["question"]);
            request.input('answer', sql.VarChar(255), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(500), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(500), post_input["subscriberchannel"]);
            request.input('personalidtype', sql.Int, post_input["personalidtype"]);
            request.input('personalidnumber', sql.VarChar(255), post_input["personalidnumber"]);
            request.input('proof_file_name', sql.VarChar(255), post_input["proof_file_name"]);
            request.execute('website_central_activation_insert_new_susbscriber').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const activation_sim_register_check = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobileno', sql.VarChar(64), post_input["mobileno"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_activation_sim_register_check').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


exports.activation_sim_register_check = activation_sim_register_check;
exports.activation_insert_new_susbscriber = activation_insert_new_susbscriber;
exports.activation_insertfreesim_registered = activation_insertfreesim_registered;