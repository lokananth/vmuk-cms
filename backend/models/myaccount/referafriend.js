var sql = require("mssql");
const dbconfig = require('../../dbconfig');

const web_rfr_transaction_list_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('user_id', sql.VarChar(32), post_input["user_id"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand',sql.Int, post_input['brand']);
            request.execute('web_rfr_transaction_list_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const web_rfr_avail_credit_info_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('user_id', sql.VarChar(32), post_input["user_id"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand',sql.Int, post_input['brand']);
            request.execute('web_rfr_avail_credit_info_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const web_get_list_subscribed_bundle_myaccount = async function (post_input) {
	return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('mobileno', sql.VarChar(22), post_input["user_id"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand',sql.Int, post_input['brand']);
            request.execute('web_get_list_subscribed_bundle_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}