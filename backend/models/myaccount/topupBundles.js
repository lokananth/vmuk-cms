var sql = require("mssql");
const dbconfig = require('../../dbconfig');

const TopupProcessByCredidCard = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(16), post_input['mobileno']);
			request.input('paymentid',sql.Int, post_input['paymentid']);
			request.input('paymentref',sql.VarChar(64), post_input['paymentref']);
			request.input('amount',sql.Float, post_input['amount']);
			request.input('ccdc_curr',sql.VarChar(4), post_input['ccdc_curr']);
			request.input('calledby',sql.VarChar(15), post_input['calledby']);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
           
            
            
            request.execute('web_topup_process_byCredidCard_myacount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const SendOnlySMS = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('message', sql.VarChar(200), post_input["message"]);

            request.input('scenerio', sql.VarChar(300), post_input["scenerio"]);
            request.execute('CRM3_insert_sms_notify_text').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const SubscribeDmukBundle = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
            request.input('usertype', sql.Int, post_input["usertype"]);
            request.input('userinfo', sql.VarChar(50), post_input["userinfo"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('paymode', sql.Int, post_input["paymode"]);
            request.input('processby', sql.VarChar(100), post_input["processby"]);
            request.input('payment_ref', sql.VarChar(50), post_input["payment_ref"]);
            request.input('bundle_price', sql.Float, post_input["bundle_price"]);
            request.execute('Dmuk_bundle_subscribe').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetDMUKBundleinfo = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('siteCode', sql.VarChar(5), post_input["siteCode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('Group_id', sql.Int, post_input["Group_id"]);

            request.execute('Dmuk_Get_Bundle_Info').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetBundleInfoDetails = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('siteCode', sql.VarChar(5), post_input["siteCode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('group_id', sql.Int, post_input["group_id"]);

            request.execute('WEB_myaccount_bundle_info_details').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetmyaccountCheckBundleEligibility = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('siteCode', sql.VarChar(5), post_input["siteCode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);

            request.execute('WEB_myaccount_check_bundle_eligibility').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Web_TopUpAmount_SP_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('siteCode', sql.VarChar(5), post_input["siteCode"]);
            request.input('brand', sql.Int, post_input["brand"]);


            request.execute('Web_TopUpAmount_SP_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const DeleteCCUpdates = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('mobileNo', sql.VarChar(20), post_input["mobileNo"]);
          


            request.execute('Myaccount_CC_Updates_Delete_Sp').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const TopupExisting = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('siteCode', sql.VarChar(4), post_input["siteCode"]);
            request.input('appCode', sql.VarChar(4), post_input["appCode"]);
            request.input('ProductCode', sql.VarChar(4), post_input["ProductCode"]);
            request.input('paymentagent', sql.VarChar(4), post_input["paymentagent"]);
            request.input('servicetype', sql.VarChar(4), post_input["servicetype"]);
            request.input('paymentmode', sql.VarChar(4), post_input["paymentmode"]);
            request.input('accountid', sql.VarChar(4), post_input["accountid"]);
            request.input('ccno', sql.VarChar(4), post_input["ccno"]);
            request.input('cvv', sql.VarChar(4), post_input["cvv"]);
            request.input('amount', sql.VarChar(4), post_input["amount"]);
            request.input('currency', sql.VarChar(4), post_input["currency"]);
            request.input('IpAddress', sql.VarChar(4), post_input["IpAddress"]);
            request.input('mobileno', sql.VarChar(4), post_input["mobileno"]);
            request.input('autoTopup', sql.VarChar(4), post_input["autoTopup"]);
            request.input('productid', sql.VarChar(4), post_input["productid"]);
            request.input('brand', sql.VarChar(4), post_input["brand"]);
          


            request.execute('TopupExisting').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetFreeSimOrderProcessStep2 = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('sitecode', sql.VarChar(10), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('mobile_no', sql.VarChar(20), post_input["mobile_no"]);
            request.input('credit_card_number', sql.VarChar(20), post_input["credit_card_number"]);
            request.input('topup_amount', sql.Float, post_input["topup_amount"]);


            request.execute('web_rfr_freesim_topup_bonus_process').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const AddNewOrder = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('email', sql.VarChar(50), post_input["email"]);
			request.input('itemId', sql.Int, post_input["itemId"]);
            request.input('orderKey', sql.VarChar(36), post_input["orderKey"]);
            request.input('orderID', sql.BigInt, post_input["orderID"]);
            request.input('orderRef', sql.VarChar(50), post_input["orderRef"]);
            request.input('Sitecode', sql.VarChar(10), post_input["Sitecode"]);
            


            request.execute('DM_Pay2_addNewOrder').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const UpdateAutoTopupStatus = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('Brand', sql.Int, post_input["Brand"]);
            request.input('Status', sql.Int, post_input["Status"]);
           


            request.execute('Web_autotopup_Status_Update').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const webMyaccountEnableAutoTopup = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('MobileNo', sql.VarChar(50), post_input["MobileNo"]);
			request.input('SubscriptionID', sql.VarChar(50), post_input["SubscriptionID"]);
            request.input('TopupAmount', sql.Money, post_input["TopupAmount"]);
            request.input('TopupCurr', sql.VarChar(3), post_input["TopupCurr"]);
            request.input('LastOrderID', sql.BigInt, post_input["LastOrderID"]);
            request.input('MinLevelID', sql.Int, post_input["MinLevelID"]);
            request.input('ProductID', sql.Int, post_input["ProductID"]);
            request.input('brand', sql.VarChar(10), post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('calledby', sql.VarChar(50), post_input["calledby"]);
            request.input('payment_ref', sql.VarChar(50), post_input["payment_ref"]);
           


            request.execute('web_myaccount_enable_auto_topup').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const Websitemyaccountcancelautorenew = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('processby', sql.VarChar(16), post_input["processby"]);
            request.input('enable_auto_renew', sql.Int, post_input["enable_auto_renew"]);

           


            request.execute('Website_myaccount_cancel_auto_renew').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

exports.TopupProcessByCredidCard = TopupProcessByCredidCard;
exports.SendOnlySMS = SendOnlySMS;
exports.SubscribeDmukBundle = SubscribeDmukBundle;
exports.GetDMUKBundleinfo = GetDMUKBundleinfo;
exports.GetBundleInfoDetails = GetBundleInfoDetails;
exports.GetmyaccountCheckBundleEligibility = GetmyaccountCheckBundleEligibility;
exports.Web_TopUpAmount_SP_myaccount = Web_TopUpAmount_SP_myaccount;
exports.DeleteCCUpdates = DeleteCCUpdates;
exports.TopupExisting = TopupExisting;
exports.GetFreeSimOrderProcessStep2 = GetFreeSimOrderProcessStep2
exports.AddNewOrder = AddNewOrder
exports.UpdateAutoTopupStatus = UpdateAutoTopupStatus
exports.webMyaccountEnableAutoTopup = webMyaccountEnableAutoTopup
exports.Websitemyaccountcancelautorenew = Websitemyaccountcancelautorenew