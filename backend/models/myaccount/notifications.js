var sql = require("mssql");
const dbconfig = require('../../dbconfig');



const web_update_personal_details_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('subscriberid',sql.Int, post_input['subscriberid']);	     
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
            request.input('title',sql.VarChar(10), post_input['title']);
            request.input('firstname',sql.VarChar(30), post_input['firstname']);
            request.input('lastname',sql.VarChar(30), post_input['lastname']);
            request.input('address1',sql.VarChar(50), post_input['address1']);
            request.input('address2',sql.VarChar(50), post_input['address2']);
            request.input('address3',sql.VarChar(50), post_input['address3']);
            request.input('city',sql.VarChar(20), post_input['city']);
            request.input('postcode',sql.VarChar(8), post_input['postcode']);
            request.input('countrycode',sql.VarChar(2), post_input['countrycode']);
            request.input('email',sql.VarChar(48), post_input['email']);
            request.input('mobilephone',sql.VarChar(15), post_input['mobilephone']);
            request.input('landlinenumber',sql.VarChar(15), post_input['landlinenumber']);
            request.input('language',sql.VarChar(20), post_input['language']);
            request.input('gender',sql.VarChar(20), post_input['gender']);
            request.input('bySMS',sql.Int, post_input['bySMS']);
            request.input('byEmail',sql.Int, post_input['byEmail']);
            request.input('birthDate',sql.VarChar(50), post_input['birthDate']);
            request.input('houseno',sql.VarChar(50), post_input['houseno']);
            request.input('sitecode',sql.VarChar(3), post_input['sitecode']);
            request.input('brand',sql.Int, post_input['brand']);
            request.input('byCall',sql.Int, post_input['byCall']);
            request.input('securityanswer',sql.VarChar(20), post_input['securityanswer']);
            
            request.execute('web_update_personal_details_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}



exports.web_update_personal_details_myaccount = web_update_personal_details_myaccount;

