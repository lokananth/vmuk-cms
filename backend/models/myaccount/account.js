var sql = require("mssql");
const dbconfig = require('../../dbconfig');


const get_payment_history = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	
			request.input('startdate',sql.VarChar(20), post_input['startdate']);			
			request.input('enddate',sql.VarChar(20), post_input['enddate']);			
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);
			request.input('brand',sql.Int, post_input['brand']);
			request.input('count',sql.Int, post_input['count']);
          
            
            
            request.execute('web_get_payment_trans_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const get_brand_type_by_mobileno = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
            
            
            request.execute('Web_get_brand_type_by_mobileno').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const get_subscribed_info = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
            
            
            request.execute('web_get_list_subscribed_bundle_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const get_paym_plan_id = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
            
            
            request.execute('paym_get_bundle_pack_balance').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const get_myaccount_subsrcibed_bundle = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
			request.input('sitecode',sql.VarChar(5), post_input['sitecode']);	     
			request.input('date_from',sql.DateTime, post_input['date_from']);	     
			request.input('date_to',sql.DateTime, post_input['date_to']);	     
			request.input('payment_type',sql.VarChar(50), post_input['payment_type']);	     
			request.input('plan',sql.VarChar(150), post_input['plan']);	     
            
            
            request.execute('website_myaccount_subsrcibed_bundle').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const personal_login_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('msisdn',sql.VarChar(16), post_input['msisdn']);	     
			request.input('passwd',sql.VarChar(20), post_input['passwd']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
	     
            
            
            request.execute('Web_personal_login_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}


exports.get_payment_history = get_payment_history;
exports.get_brand_type_by_mobileno = get_brand_type_by_mobileno;
exports.get_subscribed_info = get_subscribed_info;
exports.get_paym_plan_id = get_paym_plan_id;
exports.get_myaccount_subsrcibed_bundle = get_myaccount_subsrcibed_bundle;
exports.personal_login_myaccount = personal_login_myaccount;
