var sql = require("mssql");
const dbconfig = require('../../dbconfig');



const get_brand_type_by_mobileno = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
            
            
            request.execute('Web_get_brand_type_by_mobileno').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}



const personal_login_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('msisdn',sql.VarChar(16), post_input['msisdn']);	     
			request.input('passwd',sql.VarChar(20), post_input['passwd']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
	     
            
            
            request.execute('Web_personal_login_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}


exports.get_payment_history = get_payment_history;
exports.personal_login_myaccount = personal_login_myaccount;
