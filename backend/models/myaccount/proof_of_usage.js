var sql = require("mssql");
const dbconfig = require('../../dbconfig');

const get_call_history = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('Msisdn',sql.VarChar(20), post_input['Msisdn']);
		
			request.input('DateFrom',sql.VarChar(20), post_input['DateFrom']);
			
			request.input('DateTo',sql.VarChar(20), post_input['DateTo']);
			request.input('Type',sql.Int, post_input['Type']);
            request.input('SiteCode', sql.VarChar(10), post_input["SiteCode"]);
            request.input('count', sql.Int, post_input["count"]);
           
            
            
            request.execute('Web_Call_History_Search_SP_Myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const Webgetmaimeidtl = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('subscriberid',sql.Int, post_input['subscriberid']);	
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);			
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);
			request.input('brand',sql.Int, post_input['brand']);
          
            
            
            request.execute('web_get_ma_imei_dtl').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}



exports.get_call_history = get_call_history;
exports.Webgetmaimeidtl = Webgetmaimeidtl;
