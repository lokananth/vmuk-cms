var sql = require("mssql");
const dbconfig = require('../../dbconfig');



const webmyaccountcheckcardno = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('last6digit_cc_no',sql.VarChar(6), post_input['last6digit_cc_no']);	     
            
            
            request.execute('web_myaccount_check_card_no').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}



const GetCCUpdates = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
     
	     
            
            
            request.execute('Myaccount_CC_Updates_Get_Sp').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const web_get_personal_details_myaccount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
			request.input('subscriberid',sql.Int, post_input['subscriberid']);	     
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
     
	     
            
            
            request.execute('web_get_personal_details_myaccount').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetCcardStatus = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
     
			request.input('Mobileno',sql.VarChar(20), post_input['Mobileno']);	     
			request.input('Sitecode',sql.VarChar(4), post_input['Sitecode']);	     
          
            request.execute('Myaccount_Get_Ccard_Status_sp').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const InsertCCUpdates = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
     
			request.input('mobileNo',sql.VarChar(20), post_input['mobileNo']);	     
			request.input('sitecode',sql.VarChar(5), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
			request.input('ccNumber',sql.VarChar(20), post_input['ccNumber']);	     
			request.input('houseNo',sql.VarChar(80), post_input['houseNo']);	     
			request.input('streetName',sql.VarChar(100), post_input['streetName']);	     
			request.input('city',sql.VarChar(100), post_input['city']);	     
			request.input('country',sql.VarChar(100), post_input['country']);	     
			request.input('createdby',sql.VarChar(80), post_input['createdby']);	     
			request.input('postcode',sql.VarChar(20), post_input['postcode']);	     
          
            request.execute('Myaccount_CC_Updates_Sp').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetRegisterNumberList = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
     
			request.input('Mainmobileno',sql.VarChar(20), post_input['Mainmobileno']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
	     
          
            request.execute('CMyaccount_GetRegisterNumberList').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const GetOutstandingAmount = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
     
			request.input('mobileno',sql.VarChar(16), post_input['mobileno']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
	     
          
            request.execute('paym_get_info_outstanding_amoun').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const GetRenewalInfo = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
     
			request.input('mobileno',sql.VarChar(16), post_input['mobileno']);	     
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	     
			request.input('brand',sql.Int, post_input['brand']);	     
	     
          
            request.execute('paym_pp_renewal_info').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
const GetBundlePackBalance = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
    
			request.input('sitecode',sql.VarChar(3), post_input['sitecode']);	
			request.input('mobileno',sql.VarChar(20), post_input['mobileno']);	   
			     
			request.input('brand',sql.Int, post_input['brand']);	     
	     
          
            request.execute('paym_get_bundle_pack_balance').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}


exports.webmyaccountcheckcardno = webmyaccountcheckcardno;
exports.GetCCUpdates = GetCCUpdates;
exports.web_get_personal_details_myaccount = web_get_personal_details_myaccount;
exports.GetCcardStatus = GetCcardStatus;
exports.InsertCCUpdates = InsertCCUpdates;
exports.GetRegisterNumberList = GetRegisterNumberList;
exports.GetOutstandingAmount = GetOutstandingAmount;
exports.GetRenewalInfo = GetRenewalInfo;
exports.GetBundlePackBalance = GetBundlePackBalance;
