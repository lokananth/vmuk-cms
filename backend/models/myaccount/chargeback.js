var sql = require("mssql");
const dbconfig = require('../../dbconfig');

const web_myaccount_view_chargeback_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            //request.output('output_parameter', sql.Int);
            request.execute('web_myaccount_view_chargeback_info').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}


const web_myaccount_chargeback_unsuspend = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('paid_amount', sql.Float, post_input["paid_amount"]);
            request.input('paid_reference', sql.VarChar(150), post_input["paid_reference"]);
            request.input('calledby',sql.VarChar(50),post_input["calledby"]);
            request.input('amount_master',sql.Float,post_input["amount_master"])
            //request.output('output_parameter', sql.Int);
            request.execute('web_myaccount_chargeback_unsuspend').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const web_myaccount_check_card_no = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('last6digit_cc_no', sql.VarChar(6), post_input["paid_amount"]);
            //request.output('output_parameter', sql.Int);
            request.execute('web_myaccount_check_card_no').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const web_myaccount_validate_voucher = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('pin', sql.VarChar(15), post_input["pin"]);
            //request.output('output_parameter', sql.Int);
            request.execute('web_myaccount_validate_voucher').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const web_myaccount_burn_voucher = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
			request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('voucher_id', sql.Int, post_input["voucher_id"]);
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('calledby',sql.VarChar(50), post_input["calledby"])
            //request.output('output_parameter', sql.Int);
            request.execute('web_myaccount_burn_voucher').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

exports.web_myaccount_view_chargeback_info  = web_myaccount_view_chargeback_info;
exports.web_myaccount_chargeback_unsuspend = web_myaccount_chargeback_unsuspend;
exports.web_myaccount_check_card_no = web_myaccount_check_card_no;
exports.web_myaccount_validate_voucher = web_myaccount_validate_voucher;
exports.web_myaccount_burn_voucher = web_myaccount_burn_voucher;
