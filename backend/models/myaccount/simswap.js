var sql = require("mssql");
const dbconfig = require('../../dbconfig');

const webmyaccount_validate_iccid_info = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('iccid', sql.VarChar(20), post_input["iccid"]);
            request.input('puk',sql.VarChar(20), post_input['puk']);
            request.execute('webmyaccount_validate_iccid_info').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}

const website_myaccount_sim_swap_process = async function (post_input) {
	 return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request(); 
            request.input('msisdn', sql.VarChar(15), post_input["msisdn"]);
            request.input('iccid_new', sql.VarChar(20), post_input["iccid_new"]);
            request.input('processby', sql.VarChar(32), post_input["processby"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.execute('website_myaccount_sim_swap_process').then (function(res, recordsets, returnValue, affected) {
                //console.log(recordsets);
                resolve(res.recordset);
                setTimeout(() => reject('woops'), 500);
            }).catch(function(err) {
                reject("result unknown")
            });
        });  
    }) 
}
