var sql = require("mssql");
const dbconfig = require('../dbconfig');

const wp_get_wp_retailer_type = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
           
            request.execute('WP_Get_WP_Retailer_Type').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_service_add_check = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('reference_id', sql.VarChar(80), post_input["reference_id"]);
            request.input('svc_type', sql.VarChar(10), post_input["svc_type"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_service_add_check').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}
// Msg 15009, Level 16, State 1, Procedure sp_help, Line 66 [Batch Start Line 0]
//The object 'WP_Maintain_Website_Retailer_Sp' does not exist in database 'crm3' or is invalid for this operation.

const wp_maintain_website_retailersp = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('Retailer_Name', sql.VarChar(100), post_input["Retailer_Name"]);
            request.input('Email', sql.VarChar(100), post_input["Email"]);
            request.input('Country', sql.VarChar(100), post_input["Country"]);
            request.input('RT_Address1', sql.VarChar(100), post_input["RT_Address1"]);
            request.input('RT_Address2', sql.VarChar(100), post_input["RT_Address2"]);
            request.input('Post_Code', sql.VarChar(30), post_input["Post_Code"]);
            request.input('Prefered_Lang', sql.VarChar(30), post_input["Prefered_Lang"]);
            request.input('Mobile_No', sql.VarChar(30), post_input["Mobile_No"]);
            request.input('Landline_No', sql.VarChar(30), post_input["Landline_No"]);
            request.input('WR_University_Name', sql.VarChar(300), post_input["WR_University_Name"]);
            request.input('RT_Brand_Type', sql.Int, post_input["RT_Brand_Type"]);
            //request.output('output_parameter', sql.Int);
            request.execute('WP_Maintain_Website_Retailer_Sp').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_central_rfr_referral_topup = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobile_no', sql.VarChar(32), post_input["mobile_no"]);
            request.input('topup_amount', sql.Int, post_input["topup_amount"]);
            request.input('credit_card_number', sql.VarChar(32), post_input["credit_card_number"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_rfr_referral_topup').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}




const website_central_update_email_delivery = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
            request.input('scenerio', sql.VarChar(160), post_input["scenerio"]);
			request.input('date', sql.VarChar(50), post_input["date"]);
            request.input('email_status', sql.Int, post_input["email_status"]);           
            request.input('processby', sql.VarChar(50), post_input["processby"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_update_email_delivery').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_insert_customer_contact_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('Sitecode', sql.VarChar(5), post_input["Sitecode"]);
			request.input('First_name', sql.VarChar(150), post_input["First_name"]);
			request.input('Last_name', sql.VarChar(150), post_input["Last_name"]);
           
            request.input('Email', sql.VarChar(100), post_input["Email"]);
            request.input('Vectone_mobileno', sql.VarChar(30), post_input["Vectone_mobileno"]);
            request.input('Alternate_contactno', sql.VarChar(30), post_input["Alternate_contactno"]);
            request.input('Department', sql.VarChar(100), post_input["Department"]);
            request.input('Subject', sql.VarChar(100), post_input["Subject"]);
            request.input('Message', sql.VarChar(150), post_input["Subject"]);

            //request.output('output_parameter', sql.Int);
            request.execute('Website_insert_customer_contact_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_insert_email_log = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
			request.input('email_status', sql.Int, post_input["email_status"]);
			request.input('freesimid', sql.Int, post_input["freesimid"]);
			request.input('firstname', sql.VarChar(250), post_input["firstname"]);
           
            request.input('email', sql.VarChar(250), post_input["email"]);
            request.input('scenerio', sql.VarChar(500), post_input["scenerio"]);
            request.input('secnerio_type', sql.VarChar(500), post_input["secnerio_type"]);
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('search_date', sql.VarChar(20), post_input["search_date"]);
            request.input('email_sent_by', sql.VarChar(100), post_input["email_sent_by"]);
            request.input('email_template', sql.VarChar(500), post_input["email_template"]);

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_insert_email_log').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

exports.wp_get_wp_retailer_type = wp_get_wp_retailer_type;
exports.website_central_service_add_check = website_central_service_add_check;
exports.wp_maintain_website_retailersp = wp_maintain_website_retailersp;
exports.website_central_rfr_referral_topup = website_central_rfr_referral_topup;
exports.website_central_update_email_delivery = website_central_update_email_delivery;
exports.website_insert_customer_contact_info = website_insert_customer_contact_info;
exports.website_central_insert_email_log = website_central_insert_email_log;
