var sql = require("mssql");
const dbconfig = require('../dbconfig');

const website_central_paymonthly_get_plan = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_get_plan').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_paymonthly_insertdirectdebitorder = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('orderid', sql.BigInt, post_input["orderid"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('originatorID', sql.VarChar(100), post_input["originatorID"]);
            request.input('buildingsociety', sql.VarChar(64), post_input["buildingsociety"]);
            request.input('accountname', sql.VarChar(100), post_input["accountname"]);
            request.input('branchsortcode', sql.VarChar(10), post_input["branchsortcode"]);
            request.input('accountnumber', sql.VarChar(32), post_input["accountnumber"]);
            request.input('ddref', sql.VarChar(32), post_input["ddref"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_InsertDirectDebitOrder').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_paymonthly_write_log = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('reference_id', sql.VarChar(32), post_input["reference_id"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('subscriber_type', sql.VarChar(50), post_input["subscriber_type"]);
            request.input('activity', sql.VarChar(255), post_input["activity"]);

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_write_log').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}





const website_central_paymonthly_insertnewsubscriber = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('title', sql.VarChar(16), post_input["title"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('firstname', sql.VarChar(30), post_input["firstname"]);
            request.input('lastname', sql.VarChar(30), post_input["lastname"]);
            request.input('houseno', sql.VarChar(50), post_input["houseno"]);
            request.input('address1', sql.VarChar(50), post_input["address1"]);
            request.input('address2', sql.VarChar(50), post_input["address2"]);
            request.input('city', sql.VarChar(20), post_input["city"]);
            request.input('postcode', sql.VarChar(8), post_input["postcode"]);
            request.input('state', sql.VarChar(20), post_input["state"]);
            request.input('countrycode', sql.VarChar(2), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(15), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(15), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(15), post_input["fax"]);
            request.input('email', sql.VarChar(48), post_input["email"]);
            request.input('birthdate', sql.VarChar(20), post_input["birthdate"]);
            request.input('question', sql.VarChar(100), post_input["question"]);
            request.input('answer', sql.VarChar(50), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(50), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(50), post_input["subscriberchannel"]);
            request.input('id_type', sql.VarChar(100), post_input["id_type"]);
            request.input('id_number', sql.VarChar(100), post_input["id_number"]);
            request.input('doc_exp_Date', sql.DateTime, post_input["doc_exp_Date"]);

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_InsertNewSubscriber').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const website_central_paymonthly_update_existsubscriber = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('title', sql.VarChar(16), post_input["title"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('firstname', sql.VarChar(30), post_input["firstname"]);
            request.input('lastname', sql.VarChar(30), post_input["lastname"]);
            request.input('houseno', sql.VarChar(50), post_input["houseno"]);
            request.input('address1', sql.VarChar(50), post_input["address1"]);
            request.input('address2', sql.VarChar(50), post_input["address2"]);
            request.input('city', sql.VarChar(20), post_input["city"]);
            request.input('postcode', sql.VarChar(8), post_input["postcode"]);
            request.input('state', sql.VarChar(20), post_input["state"]);
            request.input('countrycode', sql.VarChar(2), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(15), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(15), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(15), post_input["fax"]);
            request.input('email', sql.VarChar(48), post_input["email"]);
            request.input('birthdate', sql.VarChar(20), post_input["birthdate"]);
            request.input('question', sql.VarChar(100), post_input["question"]);
            request.input('answer', sql.VarChar(50), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(50), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(50), post_input["subscriberchannel"]);
 

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_update_existSubscriber').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}



const website_central_paymonthly_freesim_create = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('subscriberid', sql.Int, post_input["subscriberid"]);
            request.input('freesimstatus', sql.Int, post_input["freesimstatus"]);
            request.input('postpaidinfo', sql.Int, post_input["postpaidinfo"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('cybersourceid', sql.VarChar(40), post_input["cybersourceid"]);
            request.input('iccid', sql.VarChar(20), post_input["iccid"]);
            request.input('loginname', sql.VarChar(32), post_input["loginname"]);
            request.input('pac', sql.VarChar(20), post_input["pac"]);
            request.input('existingno', sql.VarChar(20), post_input["existingno"]);
            request.input('promocode', sql.VarChar(32), post_input["promocode"]);
            request.input('url', sql.VarChar(1000), post_input["url"]);
            request.input('ip_address', sql.VarChar(100), post_input["ip_address"]);
            request.input('itemid', sql.Int, post_input["itemid"]);
            request.input('currency_Symbol', sql.VarChar(5), post_input["currency_Symbol"]);
            request.input('source_address', sql.VarChar(1000), post_input["source_address"]);
            request.input('sourcereg', sql.VarChar(1000), post_input["sourcereg"]);
            request.input('is_bill_gap', sql.Int, post_input["is_bill_gap"]);

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_freesim_create').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_central_paymonthly_getitembyitemid = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
			var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('itemid', sql.VarChar(20), post_input["itemid"]);
           

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_getItemByItemID').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}




const website_central_paymonthly_insert_ddaccount = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
			var request = new sql.Request();
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('paymentref', sql.VarChar(32), post_input["paymentref"]);
            request.input('sort_code', sql.VarChar(10), post_input["sort_code"]);
            request.input('account_num', sql.VarChar(32), post_input["account_num"]);
            request.input('account_name', sql.VarChar(100), post_input["account_name"]);
            request.input('branch_code', sql.VarChar(50), post_input["branch_code"]);
            request.input('first_name', sql.VarChar(150), post_input["first_name"]);
            request.input('country', sql.VarChar(150), post_input["country"]);
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('subscriberid', sql.Int, post_input["subscriberid"]);
            request.input('freesimid', sql.Int, post_input["freesimid"]);
           

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paymonthly_Insert_ddaccount').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const website_central_postpaid_subscribe = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
			var request = new sql.Request();
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('usertype', sql.Int, post_input["usertype"]);
            request.input('userinfo', sql.VarChar(30), post_input["userinfo"]);
            request.input('is_postpaid', sql.Int, post_input["is_postpaid"]);
            request.input('credinit', sql.Float, post_input["credinit"]);
            request.input('new_trffclass', sql.VarChar(100), post_input["new_trffclass"]);
            request.input('renew_delay', sql.Int, post_input["renew_delay"]);
            request.input('renew_mode', sql.Int, post_input["renew_mode"]);
            request.input('renew_datemode', sql.Int, post_input["renew_datemode"]);
            request.input('renew_chosendate', sql.DateTime, post_input["renew_chosendate"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('processby', sql.VarChar(16), post_input["processby"]);
           

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_postpaid_subscribe').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}



const website_central_pp_addneworder_detail = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
			var request = new sql.Request();
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('email', sql.VarChar(200), post_input["email"]);
            request.input('itemId', sql.Int, post_input["itemId"]);
            request.input('orderID', sql.BigInt, post_input["orderID"]);
            request.input('orderRef', sql.VarChar(50), post_input["orderRef"]);
            request.input('orderKey', sql.VarChar(36), post_input["orderKey"]);
            request.input('status', sql.Int, post_input["status"]);
            request.input('process_description', sql.VarChar(150), post_input["process_description"]);

           

            //request.output('output_parameter', sql.Int);
            request.execute('website_central_PP_addNewOrder_detail').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

// please check sp name not working

//Msg 15009, Level 16, State 1, Procedure sp_help, Line 66 [Batch Start Line 0]
//The object 'Paym_create_gocard_mandate_account' does not exist in database 'crm3' or is invalid for this operation.

const paym_create_gocard_mandate_account = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
			var request = new sql.Request();
            request.input('customerid', sql.Int, post_input["customerid"]);
            request.input('mobileno', sql.VarChar(50), post_input["mobileno"]);
            request.input('mandate_id', sql.VarChar(100), post_input["mandate_id"]);
            request.input('isvalid', sql.Int, post_input["isvalid"]);
            request.input('gocard_customerid', sql.VarChar(100), post_input["gocard_customerid"]);
            request.input('mandate_status', sql.VarChar(100), post_input["mandate_status"]);


            //request.output('output_parameter', sql.Int);
            request.execute('Paym_create_gocard_mandate_account').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}




const website_central_paym_plan_by_mobileno = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('product_name', sql.VarChar(20), post_input["product_name"]);
            request.input('lang_code', sql.VarChar(20), post_input["product_name"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('plan_upgrade', sql.Int, post_input["plan_upgrade"]);
            request.input('sitecode', sql.VarChar(20), post_input["sitecode"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paym_plan_by_mobileno').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}



const website_central_paym_change_plan_request = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('prevplan_end_date', sql.DateTime, post_input["prevplan_end_date"]);
            request.input('newplan_start_date', sql.DateTime, post_input["newplan_start_date"]);
            request.input('newplan', sql.Float, post_input["newplan"]);
            request.input('oldplan', sql.Float, post_input["oldplan"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('change_type', sql.Int, post_input["change_type"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('pay_reference', sql.VarChar(50), post_input["pay_reference"]);
            request.input('processby', sql.VarChar(100), post_input["processby"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_paym_change_plan_request').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}
exports.website_central_paymonthly_get_plan = website_central_paymonthly_get_plan;
exports.website_central_paymonthly_insertdirectdebitorder = website_central_paymonthly_insertdirectdebitorder;
exports.website_central_paymonthly_write_log = website_central_paymonthly_write_log;
exports.website_central_paymonthly_insertnewsubscriber = website_central_paymonthly_insertnewsubscriber;
exports.website_central_paymonthly_update_existsubscriber = website_central_paymonthly_update_existsubscriber;
exports.website_central_paymonthly_freesim_create = website_central_paymonthly_freesim_create;
exports.website_central_paymonthly_getitembyitemid = website_central_paymonthly_getitembyitemid;
exports.website_central_paymonthly_insert_ddaccount = website_central_paymonthly_insert_ddaccount;
exports.website_central_postpaid_subscribe = website_central_postpaid_subscribe;
exports.website_central_pp_addneworder_detail = website_central_pp_addneworder_detail;
exports.paym_create_gocard_mandate_account = paym_create_gocard_mandate_account;
exports.website_central_paym_plan_by_mobileno = website_central_paym_plan_by_mobileno;
exports.website_central_paym_change_plan_request = website_central_paym_change_plan_request;

