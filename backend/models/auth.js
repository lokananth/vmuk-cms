var sql = require("mssql");
const dbconfig = require('../dbconfig');

const auth_login = async function (post_input) {
    return new Promise((resolve, reject) => {
        const user = {
        	id:  post_input["id"],
        	username: post_input["username"],
        	email:  post_input["email"]
        }
        resolve(user);
    })
}

exports.auth_login = auth_login;
