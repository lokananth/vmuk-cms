var sql = require("mssql");
const dbconfig = require('../dbconfig');

const top_up_amount = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(30), post_input["sitecode"]);
            request.input('Website', sql.VarChar(30), post_input["website"]);
            //request.output('output_parameter', sql.Int);
            request.execute('Web_TopUpAmount_SP_myaccount_DM').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const top_up_active = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('sitecode',sql.VarChar(3), post_input["sitecode"]);
            request.input('device_type',sql.VarChar(50), post_input["device_type"]);
            request.input('browser', sql.VarChar(1000),post_input["browser"]);
            request.input('os_version',sql.VarChar(100), post_input["os_version"]);
            request.input('topup_url', sql.VarChar(1000), post_input["topup_url"]);
            request.input('processtype',sql.VarChar(150), post_input["processtype"]);
            //request.output('output_parameter', sql.Int);
            request.execute('website_central_topup_check_active_mobileno').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        }) 
    });
} 

const top_up_process_ccdc_sms = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('mobileno', sql.VarChar(16), post_input["mobileno"]);
            request.input('paymentid', sql.Int, post_input["paymentid"]);
            request.input('paymentref', sql.VarChar(64), post_input["paymentref"]);
            request.input('productid', sql.Int, post_input["productid"]);
            request.input('amount', sql.Int, post_input["amount"]);
            request.input('ccdc_curr',sql.VarChar(3), post_input["ccdc_curr"]);
            request.input('calledby',sql.VarChar(10), post_input["calledby"]);
            request.input('sitecode', sql.VarChar(3),post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('topup_url', sql.VarChar(500), post_input["topup_url"]);
            request.execute('website_central_topup_process_byccdc_with_sms_v2').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });
    });
}

const top_up_payment_failed_trans  = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {  
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('pay_reference', sql.VarChar(50), post_input["pay_reference"]);
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('pay_date', sql.DateTime, post_input["pay_date"]);
            request.input('mobileno', sql.VarChar(20), post_input["mobileno"]);
            request.input('Reason', sql.VarChar(255),  post_input["Reason"]);
            request.input('Description',sql.VarChar(255), post_input["Description"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]); 
            request.execute('website_central_topup_insert_Payment_Failed_Transaction').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });
    });
}

const top_up_auto_enable = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('MobileNo', sql.VarChar(50), post_input["MobileNo"]);
            request.input('SubscriptionID', sql.VarChar(50), post_input["SubscriptionID"]);
            request.input('TopupAmount', sql.Money, post_input["TopupAmount"]);
            request.input('TopupCurr', sql.VarChar(3), post_input["TopupCurr"]);
            request.input('LastOrderID', sql.BigInt, post_input["LastOrderID"]);
            request.input('MinLevelID', sql.Int, post_input["MinLevelID"]);
            request.input('ProductID', sql.Int, post_input["ProductID"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input("sitecode",sql.VarChar(3), post_input["sitecode"]);
            request.input("calledby",sql.VarChar(50), post_input["calledby"]);
            request.input("payment_ref",sql.VarChar(50), post_input["payment_ref"]);
            request.execute('website_central_enable_auto_topup').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });
    });
}

const top_up_auto_log = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('MobileNo', sql.VarChar(30), post_input["MobileNo"]);
            request.input('SubscriptionID', sql.VarChar(40), post_input["SubscriptionID"]);
            request.input('MinLevelID', sql.Int, post_input["MinLevelID"]);
            request.input('ItemID', sql.Int, post_input["ItemID"]);
            request.input('OrderID', sql.BigInt, post_input["OrderID"]);
            request.input("reason",sql.VarChar(250), post_input["reason"]);
            request.input("result",sql.Int, post_input["result"]);
            request.input("reasonsubs",sql.VarChar(50), post_input["reasonsubs"]); 
            request.input("description",sql.VarChar(250), post_input["description"]);
            request.input("sitecode",sql.VarChar(3), post_input["sitecode"]);
            request.input("brand",sql.Int,post_input["brand"]);
            request.execute('website_central_AutoTopupLog').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");;
            });
        });    
    });
}

const top_up_log_details = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input("mobileno", sql.VarChar(20), post_input["mobileno"]);
            request.input("paymentRef", sql.VarChar(125), post_input["paymentRef"]);
            request.input("sitecode",  sql.VarChar(3), post_input["sitecode"]);
            request.input("brand",  sql.Int, post_input["brand"]);
            request.execute('website_central_topup_get_TopupLog_details').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");;
            });
        });    
    });
}

const top_up_auto_disable = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input("MobileNo", sql.VarChar(50), post_input["MobileNo"]);
            request.input("calledby", sql.VarChar(250), post_input["calledby"]);
            request.input("brand",  sql.Int, post_input["brand"]);
            request.input("sitecode",  sql.VarChar(3), post_input["sitecode"]);
            request.execute('website_central_disable_auto_topup').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");;
            });
        });    
    });
}

const top_up_auto_payment_type_register = async function (post_input) {
    return new Promise((resolve, reject) => {  
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input("sitecode", sql.VarChar(3), post_input["sitecode"]);
            request.input("brand", sql.Int, post_input["brand"]);
            request.input("topup_type",  sql.Int, post_input["topup_type"]);
            request.input("processby",  sql.VarChar(25), post_input["processby"]);
            request.input("lastccnum",  sql.VarChar(10), post_input["lastccnum"]);
            request.input("msisdn",  sql.VarChar(20), post_input["msisdn"])
            request.execute('website_central_autotopup_payment_type_register').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
                throw new Error("Whoops!");
            }).catch(function(err) {
                throw new Error("Whoops!");
            });
        });    
    });
}

exports.top_up_amount = top_up_amount;
exports.top_up_active = top_up_active;
exports.top_up_process_ccdc_sms = top_up_process_ccdc_sms;
exports.top_up_payment_failed_trans = top_up_payment_failed_trans;
exports.top_up_auto_enable = top_up_auto_enable;
exports.top_up_auto_log = top_up_auto_log;
exports.top_up_log_details = top_up_log_details;
exports.top_up_auto_disable = top_up_auto_disable;
exports.top_up_auto_payment_type_register = top_up_auto_payment_type_register;
