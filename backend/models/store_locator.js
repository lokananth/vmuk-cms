var sql = require("mssql");
const dbconfig = require('../dbconfig');

const get_store_search_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('search_option', sql.VarChar(100), post_input["search_option"]);
            request.input('search_value', sql.VarChar(100), post_input["search_value"]);
            request.execute('website_central_store_search_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


exports.get_store_search_info = get_store_search_info;