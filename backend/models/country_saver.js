var sql = require("mssql");
const dbconfig = require('../dbconfig');

const get_registration_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('msisdn', sql.VarChar(128), post_input["msisdn"]);
            request.execute('website_central_fsb_get_registration_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const get_idx = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(128), post_input["msisdn"]);
            request.input('dest_msisdn', sql.VarChar(128), post_input["dest_msisdn"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_get_idx').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const check_balance = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(128), post_input["msisdn"]);
            request.input('price', sql.Float, post_input["price"]);
            request.input('reg_idx', sql.Int, post_input["reg_idx"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_check_balance').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const checkmsisdn_addons_info = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(128), post_input["msisdn"]);
            request.input('dest_msisdn', sql.Float, post_input["dest_msisdn"]);
            request.input('product_type', sql.VarChar(64), post_input["product_type"]);
            request.input('productprice', sql.Float, post_input["productprice"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_checkmsisdn_addons_info').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const insert_registration = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('msisdn', sql.VarChar(128), post_input["msisdn"]);
            request.input('dest_msisdn', sql.VarChar(128), post_input["dest_msisdn"]);
            request.input('veri_code', sql.VarChar(255), post_input["veri_code"]);
            request.input('productid', sql.VarChar(255), post_input["productid"]);
            request.input('landline', sql.VarChar(255), post_input["landline"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_insert_registration').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const update_registration = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('destnumber', sql.VarChar(128), post_input["destnumber"]);
            request.input('vectnumber', sql.VarChar(128), post_input["vectnumber"]);
            request.input('status', sql.Int, post_input["status"]);
            request.input('sourcereg', sql.VarChar(500), post_input["sourcereg"]);
            request.input('verificationcode', sql.VarChar(255), post_input["verificationcode"]);
            request.input('title', sql.VarChar(255), post_input["title"]);
            request.input('firstname', sql.VarChar(255), post_input["firstname"]);
            request.input('lastname', sql.VarChar(255), post_input["lastname"]);
            request.input('email', sql.VarChar(128), post_input["email"]);
            request.input('postalcode', sql.VarChar(128), post_input["postalcode"]);
            request.input('houseno', sql.VarChar(255), post_input["houseno"]);
            request.input('street', sql.VarChar(255), post_input["street"]);
            request.input('city', sql.VarChar(128), post_input["city"]);
            request.input('country', sql.VarChar(128), post_input["country"]);
            request.input('titledest', sql.VarChar(16), post_input["titledest"]);
            request.input('firstnamedest', sql.VarChar(255), post_input["firstnamedest"]);
            request.input('lastnamedest', sql.VarChar(255), post_input["lastnamedest"]);
            request.input('postcodedest', sql.VarChar(32), post_input["postcodedest"]);
            request.input('housenumberdest', sql.VarChar(32), post_input["housenumberdest"]);
            request.input('streetdest', sql.VarChar(255), post_input["streetdest"]);
            request.input('towndest', sql.VarChar(255), post_input["towndest"]);
            request.input('countrydest', sql.VarChar(255), post_input["countrydest"]);
            request.input('reg_idx', sql.Int, post_input["reg_idx"]);
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('landline', sql.VarChar(64), post_input["landline"]);
            request.input('checkaddonbundle', sql.Int, post_input["checkaddonbundle"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_update_registration').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const update_registration_pay_with_creditcard = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('destnumber', sql.VarChar(128), post_input["destnumber"]);
            request.input('vectnumber', sql.VarChar(128), post_input["vectnumber"]);
            request.input('status', sql.Int, post_input["status"]);
            request.input('sourcereg', sql.VarChar(500), post_input["sourcereg"]);
            request.input('verificationcode', sql.VarChar(255), post_input["verificationcode"]);
            request.input('title', sql.VarChar(255), post_input["title"]);
            request.input('firstname', sql.VarChar(255), post_input["firstname"]);
            request.input('lastname', sql.VarChar(255), post_input["lastname"]);
            request.input('email', sql.VarChar(128), post_input["email"]);
            request.input('postalcode', sql.VarChar(128), post_input["postalcode"]);
            request.input('houseno', sql.VarChar(255), post_input["houseno"]);
            request.input('street', sql.VarChar(255), post_input["street"]);
            request.input('city', sql.VarChar(128), post_input["city"]);
            request.input('country', sql.VarChar(128), post_input["country"]);
            request.input('titledest', sql.VarChar(16), post_input["titledest"]);
            request.input('firstnamedest', sql.VarChar(255), post_input["firstnamedest"]);
            request.input('lastnamedest', sql.VarChar(255), post_input["lastnamedest"]);
            request.input('postcodedest', sql.VarChar(32), post_input["postcodedest"]);
            request.input('housenumberdest', sql.VarChar(32), post_input["housenumberdest"]);
            request.input('streetdest', sql.VarChar(255), post_input["streetdest"]);
            request.input('towndest', sql.VarChar(255), post_input["towndest"]);
            request.input('countrydest', sql.VarChar(255), post_input["countrydest"]);
            request.input('reg_idx', sql.Int, post_input["reg_idx"]);
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('landline', sql.VarChar(64), post_input["landline"]);
            request.input('checkaddonbundle', sql.Int, post_input["checkaddonbundle"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('creditcardid', sql.BigInt, post_input["creditcardid"]);
            request.input('sorderid', sql.VarChar(255), post_input["sorderid"]);
            request.execute('website_central_fsb_update_registration_pay_with_creditcard').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const insert_payment_failed_transaction = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('pay_reference', sql.VarChar(128), post_input["pay_reference"]);
            request.input('amount', sql.Float, post_input["amount"]);
            request.input('pay_date', sql.DateTime, post_input["pay_date"]);
            request.input('mobileno', sql.VarChar(64), post_input["mobileno"]);
            request.input('Reason', sql.VarChar(255), post_input["Reason"]);
            request.input('Description', sql.VarChar(255), post_input["Description"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_Insert_Payment_Failed_Transaction').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const update_registration_pay_with_creditcard_failed = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('destnumber', sql.VarChar(64), post_input["destnumber"]);
            request.input('vectnumber', sql.VarChar(64), post_input["vectnumber"]);
            request.input('status', sql.Int, post_input["status"]);
            request.input('sourcereg', sql.VarChar(500), post_input["sourcereg"]);
            request.input('verificationcode', sql.VarChar(255), post_input["verificationcode"]);
            request.input('reg_idx', sql.Int, post_input["reg_idx"]);
            request.input('creditcardid', sql.BigInt, post_input["creditcardid"]);
            request.input('sorderid', sql.VarChar(255), post_input["sorderid"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_update_registration_pay_with_creditcard_failed').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const addon_registration = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('reg_idx', sql.Int, post_input["reg_idx"]);
            request.input('addon_msisdn', sql.VarChar(128), post_input["addon_msisdn"]);
            request.input('dest_number', sql.VarChar(128), post_input["dest_number"]);
            request.input('clitypes', sql.Int, post_input["clitypes"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_addon_registration').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const insert_activity_log = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('reg_idx', sql.Int, post_input["reg_idx"]);
            request.input('main_msisdn', sql.VarChar(128), post_input["main_msisdn"]);
            request.input('dest_msisdn', sql.VarChar(128), post_input["dest_msisdn"]);
            request.input('current_status', sql.Int, post_input["current_status"]);
            request.input('action', sql.VarChar(255), post_input["action"]);
            request.input('description', sql.VarChar(255), post_input["description"]);
            request.input('additional_info1', sql.VarChar(255), post_input["additional_info1"]);
            request.input('additional_info2', sql.VarChar(255), post_input["additional_info2"]);
            request.input('landline', sql.VarChar(64), post_input["landline"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_fsb_addon_registration').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


const hutch_bundle_subscribe = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('msisdn', sql.VarChar(128), post_input["msisdn"]);
            request.input('dest_msisdn', sql.VarChar(128), post_input["dest_msisdn"]);
            request.input('bundleid', sql.Int, post_input["bundleid"]);
            request.input('processby', sql.VarChar(128), post_input["processby"]);
            request.execute('website_central_hutch_bundle_subscribe').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


exports.get_registration_info = get_registration_info;
exports.get_idx = get_idx;
exports.check_balance = check_balance;
exports.checkmsisdn_addons_info = checkmsisdn_addons_info;
exports.insert_registration = insert_registration;
exports.update_registration = update_registration;
exports.update_registration_pay_with_creditcard = update_registration_pay_with_creditcard;
exports.insert_payment_failed_transaction = insert_payment_failed_transaction;
exports.update_registration_pay_with_creditcard_failed = update_registration_pay_with_creditcard_failed;
exports.addon_registration = addon_registration;
exports.insert_activity_log = insert_activity_log;
exports.hutch_bundle_subscribe = hutch_bundle_subscribe;