var sql = require("mssql");
const dbconfig = require('../dbconfig');

const freesim_Order = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('title', sql.VarChar(16), post_input["title"]);
            request.input('firstname', sql.NVarChar(120), post_input["firstname"]);
            request.input('lastname', sql.NVarChar(120), post_input["lastname"]);
            request.input('houseno', sql.NVarChar(200), post_input["houseno"]);
            request.input('address1', sql.NVarChar(200), post_input["address1"]);
            request.input('address2', sql.NVarChar(200), post_input["address2"]);
            request.input('city', sql.NVarChar(40), post_input["city"]);
            request.input('postcode', sql.VarChar(8), post_input["postcode"]);
            request.input('state', sql.VarChar(20), post_input["state"]);
            request.input('countrycode', sql.VarChar(2), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(15), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(15), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(15), post_input["fax"]);
            request.input('email', sql.VarChar(48), post_input["email"]);
            request.input('birthdate', sql.VarChar(20), post_input["birthdate"]);
            request.input('question', sql.VarChar(100), post_input["question"]);
            request.input('answer', sql.VarChar(50), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(50), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(50), post_input["subscriberchannel"]);
            request.input('conversion_source', sql.VarChar(50), post_input["conversion_source"]);
            request.input('country_source', sql.VarChar(100), post_input["country_source"]);
            request.input('channel_source', sql.VarChar(255), post_input["channel_source"]);
            request.input('freesimstatus', sql.Int, post_input["freesimstatus"]);
            request.input('sourceaddress', sql.VarChar(500), post_input["sourceaddress"]);
            request.input('statid', sql.Int, post_input["statid"]);
            request.input('ordersimurl', sql.VarChar(500), post_input["ordersimurl"]);
            request.input('simtype', sql.VarChar(15), post_input["simtype"]);
            request.input('nb_of_sim', sql.Int, post_input["nb_of_sim"]);
            request.input('ip_address', sql.VarChar(100), post_input["ip_address"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('fav_call_country', sql.VarChar(150), post_input["fav_call_country"]);
            request.input('landing_page_url', sql.VarChar(500), post_input["landing_page_url"]);
            request.input('last_vist_page_url', sql.VarChar(500), post_input["last_vist_page_url"]);
            request.input('old_freesimid', sql.Int, post_input["old_freesimid"]);
            request.input('promo_bundleid', sql.Int, post_input["promo_bundleid"]);
            request.input('cybersourceid', sql.VarChar(40), post_input["cybersourceid"]);
            request.execute('website_central_freesim_Order').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const referral_order_new_sim = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
			request.input('nb_of_sim', sql.Int, post_input["nb_of_sim"]);
            request.input('title', sql.VarChar(16), post_input["title"]);
            request.input('firstname', sql.VarChar(30), post_input["firstname"]);
            request.input('lastname', sql.VarChar(30), post_input["lastname"]);
            request.input('houseno', sql.VarChar(50), post_input["houseno"]);
            request.input('address1', sql.VarChar(50), post_input["address1"]);
            request.input('address2', sql.VarChar(50), post_input["address2"]);
            request.input('city', sql.VarChar(20), post_input["city"]);
            request.input('postcode', sql.VarChar(8), post_input["postcode"]);
            request.input('state', sql.VarChar(20), post_input["state"]);
            request.input('countrycode', sql.VarChar(2), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(15), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(15), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(15), post_input["fax"]);
            request.input('email', sql.VarChar(48), post_input["email"]);
            request.input('birthdate', sql.VarChar(20), post_input["birthdate"]);
            request.input('question', sql.VarChar(100), post_input["question"]);
            request.input('answer', sql.VarChar(50), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(50), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(50), post_input["subscriberchannel"]);
            request.input('conversion_source', sql.VarChar(50), post_input["conversion_source"]);
            request.input('country_source', sql.VarChar(100), post_input["country_source"]);
            request.input('channel_source', sql.VarChar(255), post_input["channel_source"]);
            request.input('freesimstatus', sql.Int, post_input["freesimstatus"]);
            request.input('sourceaddress', sql.VarChar(500), post_input["sourceaddress"]);
            request.input('statid', sql.Int, post_input["statid"]);
            request.input('ordersimurl', sql.VarChar(500), post_input["ordersimurl"]);
            request.input('referrer', sql.VarChar(15), post_input["referrer"]);
            request.input('ReferralType', sql.Int, post_input["ReferralType"]);
            request.input('clickid', sql.VarChar(32), post_input["clickid"]);
            request.input('ip_address', sql.VarChar(100), post_input["ip_address"]);
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.execute('website_central_referral_order_new_sim').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const create_sim_with_credit = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(5), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('title', sql.VarChar(16), post_input["title"]);
            request.input('firstname', sql.VarChar(30), post_input["firstname"]);
            request.input('lastname', sql.VarChar(30), post_input["lastname"]);
            request.input('houseno', sql.VarChar(50), post_input["houseno"]);
            request.input('address1', sql.VarChar(50), post_input["address1"]);
            request.input('address2', sql.VarChar(50), post_input["address2"]);
            request.input('city', sql.VarChar(20), post_input["city"]);
            request.input('postcode', sql.VarChar(8), post_input["postcode"]);
            request.input('state', sql.VarChar(255), post_input["state"]);
            request.input('countrycode', sql.VarChar(2), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(15), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(15), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(15), post_input["fax"]);
            request.input('email', sql.VarChar(48), post_input["email"]);
            request.input('birthdate', sql.VarChar(20), post_input["birthdate"]);
            request.input('question', sql.VarChar(100), post_input["question"]);
            request.input('answer', sql.VarChar(50), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(50), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(50), post_input["subscriberchannel"]);
            request.input('conversion_source', sql.VarChar(50), post_input["conversion_source"]);
            request.input('country_source', sql.VarChar(100), post_input["country_source"]);
            request.input('channel_source', sql.VarChar(255), post_input["channel_source"]);
            request.input('simtype', sql.VarChar(15), post_input["simtype"]);
            request.input('freesimstatus', sql.Int, post_input["freesimstatus"]);
            request.input('freesimtype', sql.BigInt, post_input["freesimtype"]);
            request.input('cybersourceid', sql.VarChar(30), post_input["cybersourceid"]);
            request.input('ip_address', sql.VarChar(50), post_input["ip_address"]);
            request.input('ordersim_url', sql.VarChar(150), post_input["ordersim_url"]);
            request.input('source_address', sql.VarChar(150), post_input["source_address"]);
            request.execute('website_central_create_sim_with_credit').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const threeds_payment_log_insert = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('mobileno', sql.VarChar(32), post_input["mobileno"]);
            request.input('REFERENCE_ID', sql.VarChar(64), post_input["REFERENCE_ID"]);
            request.input('Pay_step', sql.Int, post_input["Pay_step"]);
            request.input('Pay_code', sql.Int, post_input["Pay_code"]);
            request.input('Pay_decision', sql.VarChar(100), post_input["Pay_decision"]);
            request.input('Pay_description', sql.VarChar(500), post_input["Pay_description"]);
            request.input('Pay_stage', sql.VarChar(150), post_input["Pay_stage"]);
            request.execute('website_central_threeds_payment_log_insert').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const freesim_with_bundle_topup = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.Int, post_input["brand"]);
            request.input('FreeSimId', sql.VarChar(64), post_input["FreeSimId"]);
            request.input('Ordered_Date', sql.DateTime, post_input["Ordered_Date"]);
            request.input('SubscriberId', sql.Int, post_input["SubscriberId"]);
            request.input('freesim_type', sql.Int, post_input["freesim_type"]);
            request.input('Bundle_Id', sql.Int, post_input["Bundle_Id"]);
            request.input('FreeSim_Status', sql.Int, post_input["FreeSim_Status"]);
            request.input('Payment_Ref', sql.VarChar(64), post_input["Payment_Ref"]);
            request.input('Topup_amt', sql.Float, post_input["Topup_amt"]);
            request.input('prodcutid', sql.Int, post_input["prodcutid"]);
            request.input('currency', sql.VarChar(30), post_input["currency"]);
            request.input('processby', sql.VarChar(30), post_input["processby"]);
            request.input('purchase_type', sql.Int, post_input["purchase_type"]);
            request.input('promo_code', sql.VarChar(64), post_input["promo_code"]);
            request.input('promo_flag', sql.Int, post_input["promo_flag"]);
            request.input('sim_price', sql.Float, post_input["sim_price"]);
            request.input('addon_bundle_id', sql.Int, post_input["addon_bundle_id"]);
            request.input('dest_number', sql.VarChar(64), post_input["dest_number"]);
            request.input('bundle_type', sql.Int, post_input["bundle_type"]);
            request.input('activation_flag', sql.Int, post_input["activation_flag"]);
            request.input('cc_number', sql.VarChar(64), post_input["cc_number"]);
            request.input('called_by', sql.VarChar(64), post_input["called_by"]);
            request.execute('website_central_freesim_with_bundle_topup').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

exports.freesim_Order = freesim_Order;
exports.referral_order_new_sim = referral_order_new_sim;
exports.create_sim_with_credit = create_sim_with_credit;
exports.threeds_payment_log_insert = threeds_payment_log_insert;
exports.freesim_with_bundle_topup = freesim_with_bundle_topup;
