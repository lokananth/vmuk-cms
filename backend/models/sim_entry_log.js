var sql = require("mssql");
const dbconfig = require('../dbconfig');

const create_sim_entry_log = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('firstname', sql.VarChar(255), post_input["firstname"]);
            request.input('lastname', sql.VarChar(255), post_input["lastname"]);
            request.input('email', sql.VarChar(255), post_input["email"]);
            request.input('simorder_type', sql.VarChar(255), post_input["simorder_type"]);
            request.input('ip_address', sql.VarChar(64), post_input["ip_address"]);
            request.input('device_type', sql.VarChar(255), post_input["device_type"]);
            request.input('browser_type', sql.VarChar(255), post_input["browser_type"]);
            request.input('url', sql.VarChar(255), post_input["url"]);
            request.execute('website_central_create_sim_entry_log').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


exports.create_sim_entry_log = create_sim_entry_log;