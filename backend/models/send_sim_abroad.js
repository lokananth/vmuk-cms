var sql = require("mssql");
const dbconfig = require('../dbconfig');

const simorder_get_price = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.VarChar(16), post_input["brand"]);
            request.input('Country_Code', sql.VarChar(32), post_input["Country_Code"]);
            request.execute('website_central_simorder_get_price').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const simorder_check_address = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.VarChar(16), post_input["brand"]);
            request.input('houseno', sql.VarChar(255), post_input["houseno"]);
            request.input('postcode', sql.VarChar(32), post_input["postcode"]);
            request.execute('website_central_simorder_check_address').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const simorder_subsid_create = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.VarChar(16), post_input["brand"]);
            request.input('title', sql.VarChar(8), post_input["title"]);
            request.input('firstname', sql.VarChar(255), post_input["firstname"]);
            request.input('lastname', sql.VarChar(255), post_input["lastname"]);
            request.input('houseno', sql.VarChar(255), post_input["houseno"]);
            request.input('address1', sql.VarChar(255), post_input["address1"]);
            request.input('address2', sql.VarChar(255), post_input["address2"]);
            request.input('city', sql.VarChar(128), post_input["city"]);
            request.input('postcode', sql.VarChar(32), post_input["postcode"]);
            request.input('state', sql.VarChar(128), post_input["state"]);
            request.input('countrycode', sql.VarChar(32), post_input["countrycode"]);
            request.input('telephone', sql.VarChar(64), post_input["telephone"]);
            request.input('mobilephone', sql.VarChar(64), post_input["mobilephone"]);
            request.input('fax', sql.VarChar(64), post_input["fax"]);
            request.input('email', sql.VarChar(128), post_input["email"]);
            request.input('birthdate', sql.VarChar(128), post_input["birthdate"]);
            request.input('question', sql.VarChar(255), post_input["question"]);
            request.input('answer', sql.VarChar(255), post_input["answer"]);
            request.input('subscribertype', sql.Int, post_input["subscribertype"]);
            request.input('sourcereg', sql.VarChar(500), post_input["sourcereg"]);
            request.input('subscriberchannel', sql.VarChar(500), post_input["subscriberchannel"]);
            request.input('conversion_source', sql.VarChar(500), post_input["conversion_source"]);
            request.input('country_source', sql.VarChar(500), post_input["country_source"]);
            request.input('channel_source', sql.VarChar(500), post_input["channel_source"]);
            request.input('simtype', sql.VarChar(64), post_input["simtype"]);
            request.execute('website_central_simorder_subsid_create').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}

const freesim_create = async function (post_input) {
    return new Promise((resolve, reject) => {    
        sql.connect(dbconfig, function (err) {
            if (err) console.log(err);
            var request = new sql.Request();
            request.input('sitecode', sql.VarChar(3), post_input["sitecode"]);
            request.input('brand', sql.VarChar(16), post_input["brand"]);
            request.input('subscriberid', sql.Int, post_input["subscriberid"]);
            request.input('freesimstatus', sql.Int, post_input["freesimstatus"]);
            request.input('sourceaddress', sql.VarChar(500), post_input["sourceaddress"]);
            request.input('statid', sql.Int, post_input["statid"]);
            request.input('ordersimurl', sql.VarChar(500), post_input["ordersimurl"]);
            request.input('referrer', sql.VarChar(255), post_input["referrer"]);
            request.input('ip_address', sql.VarChar(64), post_input["ip_address"]);
            request.input('cybersourceid', sql.VarChar(32), post_input["cybersourceid"]);
            request.execute('website_central_SEND_SIM_ABROAD_freesim_create').then (function(res, recordsets, returnValue, affected) {
                resolve(res);
            }).then((result) => {
              throw new Error("Error!"); // rejects the promise
            }).catch(function(err) {
                reject("Result Unknown");
            });
        });  
    }) 
}


exports.simorder_get_price = simorder_get_price;
exports.simorder_check_address = simorder_check_address;
exports.simorder_subsid_create = simorder_subsid_create;
exports.freesim_create = freesim_create;